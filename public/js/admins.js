function activation_admin(id,key,url) {
alert(id+" - "+key);
}
function delete_admin(id,key,url,idx) {
	var string_data="delete_admin=true&id="+id+"&key="+key+"&url="+url;
	$.ajax({
		url:url,
		type:'post',
		dataType:'json',
		data:string_data,
		success:function(data){
			if(data.return)
			{
				tbl.row($("#td-"+idx).parents('tr')).remove().draw();
			}
			else
			{
				if (!data.login) {
					$("#body-update-admin").html(data.message);
					$("#update-admin-modal .modal-footer").fadeOut("fast");
					$("#update-admin-modal .overlay").fadeOut("fast",function(){
						$("#update-admin-modal .modal-body").fadeIn("fast");
					});
					var addr=document.location.href;
					var string_data="redirectto="+addr;
					$("#update-admin-modal .modal-title").html("Problem!");
					$.post( "//localhost/mvc-php/dashboard/ajax/redirectto", { redirectto: addr});
				}
			}
		}
		});
}
function update_admin(url)
{
	var inputs = $("#frm-update").serialize();
	ajax_loading("update-admin-modal .output","loading","Loading...");
	$.ajax({
		url:url,
		type:'post',
		dataType:'json',
		data:inputs,
		success:function(data){
			if(data.return)
			{
				ajax_loading("update-admin-modal .output","success",data.message);
			}
			else
			{
				if (!data.login) {
					$("#body-update-admin").html(data.message);
					$("#update-admin-modal .modal-footer").fadeOut("fast");
					$("#update-admin-modal .overlay").fadeOut("fast",function(){
						$("#update-admin-modal .modal-body").fadeIn("fast");
					});
					var addr=document.location.href;
					var string_data="redirectto="+addr;
					$("#update-admin-modal .modal-title").html("Problem!");
					$.post( "//localhost/mvc-php/dashboard/ajax/redirectto", { redirectto: addr});	
				}
				else
				{
					ajax_loading("update-admin-modal .output","error",data.message);
				}
			}
		}
	});
}
function clear_add_admin() {
	$("#frm-add input").val('');
	ajax_loading("add-admin-modal .output","hidden","");
}
function add_admin(url)
{
	$.ajax({
		url:"//localhost/mvc-php/dashboard/ajax/csrf_token",
		type:'post',
		data:{},
		success:function(data){
			$("#frm-add input[name=csrf_token]").remove();
			$("#frm-add").append(data);	
			var inputs = $("#frm-add").serialize();
			ajax_loading("add-admin-modal .output","loading","Loading...");
			$.ajax({
				url:url,
				type:'post',
				dataType:'json',
				data:inputs,
				success:function(data){
					if(data.return)
					{
						tbl.row.add([
				            data.admin.fullname,
				            data.admin.username,
				            data.admin.email,
				            data.admin.mobile,
				            data.admin.access_level,
				            data.admin.actions
				        ]).draw();
						ajax_loading("add-admin-modal .output","success",data.message);
					}
					else
					{
						if (!data.login) {
							$("#add-admin-body").html(data.message);
							$("#add-admin-modal .modal-footer").fadeOut("fast");
							$("#add-admin-modal .overlay").fadeOut("fast",function(){
								$("#add-admin-modal .modal-body").fadeIn("fast");
							});
							var addr=document.location.href;
							var string_data="redirectto="+addr;
							$("#update-admin-modal .modal-title").html("Login");
							$.post( "//localhost/mvc-php/dashboard/ajax/redirectto", { redirectto: addr});
						}
						else
						{
							ajax_loading("add-admin-modal .output","error",data.message);	
						}
					}
				}
			});
		






		}
	});

}
function fetch_admin_info(id,key,url){
	ajax_loading('update-admin-modal .output','hidden','');
	var string_data="fetch_admin_info=true&id="+id+"&key="+key;
	$("#update-admin-modal .modal-footer").fadeIn(1);
	$("#update-admin-modal .modal-footer .btn-primary").attr("disabled","disabled");
	$("#update-admin-modal .modal-title").html("...");
	$("#update-admin-modal .modal-body").fadeOut(1,function(){
			$("#update-admin-modal .overlay").fadeIn("fast",function(){
	$.ajax({
		url:url,
		type:'post',
		dataType:'json',
		data:string_data,
		success:function(data){
			if(data.return)
			{
				$("#update-admin-modal .modal-title").html(data.admin.modal_title);
				$("#firstName").val(data.admin.first_name);
				$("#csrf_token").val(data.admin.csrf_token);
				$("#lastName").val(data.admin.last_name);
				$("#email").val(data.admin.email);
				$("#mobile").val(data.admin.mobile);
				$("#username").val(data.admin.username);
				$("#id").val(data.admin.encrypted_id);
				$("#key").val(data.admin.encrypted_key);
				$("#update-admin-modal .overlay").fadeOut("fast",function(){
					$("#update-admin-modal .modal-body").fadeIn("fast");
					$("#update-admin-modal .modal-footer .btn-primary").removeAttr("disabled");
				});
			}
			else
			{
				$("#body-update-admin").html(data.message);
				$("#update-admin-modal .modal-footer").fadeOut("fast");
				$("#update-admin-modal .overlay").fadeOut("fast",function(){
					$("#update-admin-modal .modal-body").fadeIn("fast");
				});
				var addr=document.location.href;
				var string_data="redirectto="+addr;
				$("#update-admin-modal .modal-title").html("Problem!");
				$.post( "//localhost/mvc-php/dashboard/ajax/redirectto", { redirectto: addr});
			}
		}
		});

			
});
		});
}
function ajax_loading(id,type,text) {
	$("#"+id).css('display','block');
	if(type=="loading")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-info');
	}
	else if(type=="error")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-danger');
	}
	else if(type=="success")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-success');
	}
	else if(type=="warning")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-warning');
	}
	else if(type=="hidden")
	{
		$("#"+id).fadeOut('fast');
	}
	$("#"+id).html(text);
}