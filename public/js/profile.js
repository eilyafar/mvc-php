function changePassword(url) {
	var old_password=$("#inp-old-password").val();
	var new_password=$("#inp-new-password").val();
	var confirm_password=$("#inp-confirm-password").val();
	if(old_password.length > 5 && new_password.length > 5 && new_password==confirm_password)
	{
		var string_data="change_password=true&old_password="+old_password+"&new_password="+new_password+"&confirm_password="+confirm_password;
		ajax_loading('change-password-output','loading','Please wait...');
		$.ajax({
			url:url,
			type:'post',
			dataType:'json',
			data:string_data,
			success:function(data) {
				if(data.return)
				{
					$("#inp-old-password").val('');
					$("#inp-new-password").val('');
					$("#inp-confirm-password").val('');
					ajax_loading('change-password-output','success',data.message);
				}
				else
				{

					ajax_loading('change-password-output','error',data.message);
				}
			},
			error:function() {
				ajax_loading('change-password-output','error','Cannt connect to server.');
			}
		});
	}
	else
	{
		$("#inp-password").val('');
		ajax_loading('change-password-output','error','Password must be of minimum 6 characters length and same passwords.');
	}
}
function checkPassword(url) {
	var password=$("#inp-password").val();
	if(password.length > 5)
	{
		var string_data="check_password=true&password="+password;
		ajax_loading('check-password-output','loading','Please wait...');
		$.ajax({
			url:url,
			type:'post',
			dataType:'json',
			data:string_data,
			success:function(data) {
				if(data.return)
				{
					ajax_loading('check-password-output','success',data.message);
					document.getElementById('frm-update').submit();
				}
				else
				{
					$("#inp-password").val('');
					ajax_loading('check-password-output','error',data.message);
				}
			},
			error:function() {
				$("#inp-password").val('');
				ajax_loading('check-password-output','error','Cannt connect to server.');
			}
		});
	}
	else
	{
		$("#inp-password").val('');
		ajax_loading('check-password-output','error','Password must be of minimum 6 characters length.');
	}
}
function ajax_loading(id,type,text) {
	$("#"+id).css('display','block');
	if(type=="loading")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-info');
	}
	else if(type=="error")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-danger');
	}
	else if(type=="success")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-success');
	}
	else if(type=="warning")
	{
		$("#"+id).removeClass('alert-info');
		$("#"+id).removeClass('alert-success');
		$("#"+id).removeClass('alert-warning');
		$("#"+id).removeClass('alert-danger');
		$("#"+id).addClass('alert-warning');
	}
	$("#"+id).html(text);
}