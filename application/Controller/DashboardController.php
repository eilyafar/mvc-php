<?php
namespace Mini\Controller;

use Mini\Core\Redirect;
use Mini\Core\Securimage;
use Mini\Core\Session;
use Mini\Core\Template;
use Mini\Model\Admin;
use Mini\Model\Login;

class DashboardController
{
    public function index()
    {

        $login = new Login();
        if ($login->isUserLoggedIn()) {
            $header = Template::craeteHeader([[]], [['Dashboard' => DASHBOARD]], 'Dashboard');
            $title  = "Dashboard";
            require APP . 'view/_templates/dashboard_header.php';
            require APP . 'view/dashboard/index.php';
            require APP . 'view/_templates/dashboard_footer.php';
        } else {
            Redirect::to(LOGIN);
        }
    }
    public function admins()
    {

        $login = new Login();
        if ($login->isUserLoggedIn()) {
            $header  = Template::craeteHeader([['Dashboard' => DASHBOARD]], [['Admins' => 'javascript:void(0)']], 'Admins');
            $title   = "Admins";
            $scripts = ['js' => ['datatables', 'admins'], 'css' => ['datatables']];
            require APP . 'view/_templates/dashboard_header.php';
            require APP . 'view/dashboard/admins/index.php';
            require APP . 'view/_templates/dashboard_footer.php';
        } else {
            Redirect::to(LOGIN);
        }
    }
    public function ajax($parameter)
    {
        $login = new Login();
        if ($login->isUserLoggedIn()) {
            switch ($parameter) {
                case 'csrf_token':
                    echo Template::csrf();
                    break;
                case 'confirm_password':
                    $login = new Login();
                    $login->validatePassword();
                    break;
                case 'change_password':
                    $admin = new Admin();
                    $admin->changePassword();
                    break;
                case 'fetch_admin':
                    $admin = new Admin();
                    $admin->fetchAdmin();
                    break;
                case 'update_admin':
                    $admin = new Admin();
                    $admin->updateAdmin();
                    break;
                case 'delete_admin':
                    $admin = new Admin();
                    $admin->deleteAdmin();
                    break;
                case 'add_admin':
                    $admin = new Admin();
                    $admin->addAdmin();
                    break;
                default:
                    break;
            }
        } else {

            if ($parameter == "redirectto") {
                $session                = new Session();
                $_SESSION['redirectto'] = $_POST['redirectto'];
                exit;
            }
            $ret = ["return" => false,"loggin"=>false, "message" => '<p class="text-red">Your session has expired. Please log in again</p>
            <form action="' . URL . 'dashboard/checklogin" method="post">
              <div class="form-group has-feedback">
                <input type="hidden" name="login">
                <input type="text" class="form-control" placeholder="Username"  name="username" maxlength="32" required style="border-radius: 4px;">
                <span class="fa fa-user form-control-feedback"></span>
              </div>
              ' . Template::csrf() . '
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password"
                name="password" maxlength="32" required style="border-radius: 4px;">
                <span class="fa fa-lock form-control-feedback"></span>
              </div>
              <hr>
              <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                  <button type="submit" name="login" class="btn btn-primary btn-block ">Sign In</button>
                </div>
              </div>
            </form>', ];
            echo json_encode($ret);
        }
    }
    public function profile($requests = null)
    {
        $admin = new Admin();
        if ($requests == null) {
            $login = new Login();
            if ($login->isUserLoggedIn()) {
                $header  = Template::craeteHeader([['Dashboard' => DASHBOARD]], [['My Profile' => 'javascript:void(0)']], 'My Profile');
                $title   = "Dashboard";
                $scripts = ['js' => ['profile']];
                require APP . 'view/_templates/dashboard_header.php';
                require APP . 'view/dashboard/profile/index.php';
                require APP . 'view/_templates/dashboard_footer.php';
            } else {
                Redirect::to(LOGIN);
            }
        } elseif ($requests == "update") {
            $admin->updateAdmin();
        }
    }
    public function login($captcha = false)
    {
        $login = new Login();
        if ($login->isUserLoggedIn()) {
            Redirect::to(DASHBOARD);
        } else {
            if ($captcha) {
                $img = new Securimage();
                require APP . 'view/dashboard/login/captcha.php';
            } else {
                $title = "Login";
                require APP . 'view/_templates/login_header.php';
                require APP . 'view/dashboard/login/index.php';
                require APP . 'view/_templates/login_footer.php';
            }
        }
    }
    public function recover($token = null, $email = null)
    {
        $login = new Login();
        $login->recover($token, $email);
    }
    public function error()
    {
        $header = Template::craeteHeader([['Dashboard' => DASHBOARD]],
            [['Error' => 'javascript:void(0)']], '<span class="text-danger">Oops!</span> Page not found.');
        require APP . 'view/_templates/dashboard_header.php';
        require APP . 'view/dashboard/error/index.php';
        require APP . 'view/_templates/dashboard_footer.php';
    }
    public function checkLogin()
    {
        $login = new Login();
        $login->checkLogin($_POST);
    }
    public function checkEmail()
    {
        $login = new Login();
        $login->checkEmail($_POST);
    }
    public function logout()
    {
        $login = new Login();
        $login->doLogout();
    }
}
