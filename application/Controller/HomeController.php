<?php

/**
 * Class HomeController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;
use Mini\Core\Redirect;
use Mini\Core\Template;
<<<<<<< HEAD
=======
use Mini\Model\Install;
>>>>>>> 022c2db7d48ef2a4084d88db17a8c2d31de82c0f
use mysqli;
class HomeController
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
        if (JUST_DASHBOARD) {
            Redirect::to(DASHBOARD);
        }
        $header = Template::craeteHeader([[]], [['Home' => HOME]], 'Welcome to php mvc project :)');
        $title  = "Welcome";
        require APP . 'view/_templates/home_header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/_templates/home_footer.php';
    }

    public function error()
    {
        $header = Template::craeteHeader([['Home' => HOME]],
            [['Error' => 'javascript:void(0)']], '<span class="text-danger">Oops!</span> Page not found.');
        $title = "Error";
        require APP . 'view/_templates/home_header.php';
        require APP . 'view/home/error/index.php';
        require APP . 'view/_templates/home_footer.php';
    }

}
