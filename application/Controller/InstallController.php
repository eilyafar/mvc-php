<?php
namespace Mini\Controller;
use Mini\Core\Template;
use Mini\Core\Schema;
use Mini\Model\Install;
class InstallController
{
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        $header=Template::craeteHeader([],[],
        '<H1 class="text-success" style="font-family:Verdana">Installation</H1><small class="text-warning" style="font-family:Verdana">
        Welcome to php mvc project installation, powred by <a href="mailto:mahmoudian.vahid@gmail.com">Vahid Mahmoudian</a>:)</small>');
        $title="Install";
        $isInstalled=Install::isInstalled();
        if(!$isInstalled)
        {
            Install::buildTables();
        }
        require APP . 'view/_templates/home_header.php';
        require APP . 'view/install/index.php';
        require APP . 'view/_templates/home_footer.php';

    }
    public function register()
    {
        if(isset($_POST))
        {
            Install::register($_POST);
        }
    }

}
