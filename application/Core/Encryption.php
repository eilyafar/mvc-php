<?php
namespace Mini\Core;
/**
* 
*/
class Encryption
{

	public static function encrypt($data, $key)
    {
        
        $key       = substr(hash('sha256', ENCRYPTION_SALT . $key . ENCRYPTION_SALT), 0, 32);
        $iv_size   = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv        = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_ECB, $iv));
        return $encrypted;
    }
    public static function decrypt($datax, $key)
    {
        $data      = str_replace(" ", "+", $datax);
        $key       = substr(hash('sha256', ENCRYPTION_SALT . $key . ENCRYPTION_SALT), 0, 32);
        $iv_size   = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv        = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($data), MCRYPT_MODE_ECB, $iv);
        $decrypted = rtrim($decrypted, "\0");
        return $decrypted;
    }
}

?>