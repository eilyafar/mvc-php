<?php
namespace Mini\Core;

use Mini\Core\Session;

class Template
{
    public static function craeteHeader($completeds, $actives, $title)
    {
        $li_completeds = '';
        $li_actives    = '';
        $login         = null;

        if (isset($_SESSION['multi_login']) and $_SESSION['multi_login']) {
            $data  = json_decode($_SESSION['multi_login']);
            $login = '<div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Warning!</h4>
                Your account was logged into from a new browser or device [' . $data->ip . ']. review the login
              </div>';
        }

        if (count($completeds) and count($actives)) {
            foreach ($completeds as $completed) {
                foreach ($completed as $key => $value) {
                    $li_completeds .= '<li class="completed"><a href="' . $value . '">' . $key . '</a></li>';
                }
            }
            $li_actives = '';
            foreach ($actives as $active) {
                foreach ($active as $key => $value) {
                    $li_actives .= '<li class="active"><a href="' . $value . '">' . $key . '</a></li>';
                }
            }
        }
        if (!count($completeds) and !count($actives)) {
            return '
			<div class="row">
				<h2>' . $title . '</h2>
			</div>
			<hr>' . $login;
        }
        return '
		<div class="row">
			<h2>' . $title . '</h2>
			<hr>
			<ul class="breadcrumb">
				' . $li_completeds . '
				' . $li_actives . '
			</ul>' . $login . '
		</div>';
    }

    public static function csrf($type = 'hidden')
    {
        //if (!session_id()) @session_start();
        $session                = new Session();
        $token                  = CSRF_TOKEN;
        $input                  = '<input type="' . $type . '" name="csrf_token" value="' . $token . '" />';
        $_SESSION['csrf_token'] = $token;
        return $input;
    }

}
