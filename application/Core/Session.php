<?php
namespace Mini\Core;

/**
 * A PHP session handler to keep session data within a MySQL database
 *
 * @author     Manuel Reinhard <manu@sprain.ch>
 * @link        https://github.com/sprain/PHP-MySQL-Session-Handler
 */
class Session
{
    protected $db;
    public function __construct($session_name = '_s', $secure = false)
    {
        $this->db = new PDODb();
        session_set_save_handler(array($this, 'open'),
            array($this, 'close'),
            array($this, 'read'),
            array($this, 'write'),
            array($this, 'destroy'),
            array($this, 'gc'));
        register_shutdown_function('session_write_close');
        $this->startSession($session_name, $secure);

    }
    public function startSession($session_name, $secure)
    {
        $httponly     = true;
        $session_hash = 'sha512';
        if (in_array($session_hash, hash_algos())) {
            ini_set('session.hash_function', $session_hash);
        }
        ini_set('session.hash_bits_per_character', 5);
        ini_set('session.use_only_cookies', 1);
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
        session_name($session_name);
        @session_start();
        @session_regenerate_id(true);
    }
    public function open()
    {
        $limit = time() - SESSION_TIMEOUT;
        $this->db->where('timestamp < ' . $limit);
        return $this->db->delete('sessions');
    }
    public function close()
    {
        return true;
    }
    public function read($id)
    {
        $this->db->where('id', $id);
        $key = $this->getKey($id);
        if ($this->db->has('sessions')) {
            $this->db->where('id', $id);
            return $this->decrypt($this->db->getValue('sessions', 'data'), $key);
        }

        return false;
    }
    public function write($id, $data)
    {
        $key            = $this->getKey($id);
        $encrypted_data = $this->encrypt($data, $key);
        $data           = ['id' => $id, 'data' => $encrypted_data, 'timestamp' => time(), 'session_key' => $key];
        $this->db->where('id', $id);
        if ($this->db->has('sessions')) {
            $this->db->where('id', $id);
            return $this->db->update('sessions', $data);
        }

        return $this->db->insert('sessions', $data);

    }
    public function destroy($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('sessions');
    }
    public function gc($max)
    {
        $this->db->where('timestamp < ' . time() - intval($max));
        return $this->db->delete('sessions');
    }
    public function getkey($id)
    {
        $this->db->where('id', $id);
        if ($this->db->has('sessions')) {
            $this->db->where('id', $id);
            return $this->db->getValue('sessions', 'session_key');
        } else {
            $random_key = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            return $random_key;
        }
    }
    private function encrypt($data, $key)
    {
        $key       = substr(hash('sha256', SESSION_SALT . $key . SESSION_SALT), 0, 32);
        $iv_size   = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv        = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_ECB, $iv));
        return $encrypted;
    }
    private function decrypt($data, $key)
    {
        $key       = substr(hash('sha256', SESSION_SALT . $key . SESSION_SALT), 0, 32);
        $iv_size   = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv        = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($data), MCRYPT_MODE_ECB, $iv);
        $decrypted = rtrim($decrypted, "\0");
        return $decrypted;
    }
}
