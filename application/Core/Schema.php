<?php
namespace Mini\Core;

use Mini\Core\PDODb;

/**
 * Query Builder
 */
class Schema
{
    private $query       = [];
    private $charSet     = "utf8";
    private $engineValue = "INNODB";
    private $tableName   = null;
    private $db          = null;
    public function __construct($name)
    {
        $this->tableName = $name;
        $this->db        = new PDODb();
    }
    public static function dropTable($table)
    {
        if (self::tableExists($table)) {
            return $this->execute("DROP TABLE IF EXISTS `$table`");
        }
        return false;
    }
    public static function tableExists($table)
    {
        $db = new PDODb();
        return $db->tableExists($table);
    }
    public function bigIncrements($id)
    {
        $this->query[] = "`{$id}` BIGINT %SIGNED% %UNIQUE% %NULLABLE% AUTO_INCREMENT";
        return $this;
    }
    public function bigInteger($id)
    {
        $this->query[] = "`{$id}` BIGINT %SIGNED% %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function binary($data)
    {
        $this->query[] = "`{$data}` BLOB %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function boolean($confirmed)
    {
        $this->query[] = "`{$confirmed}` ENUM('true','false') %UNIQUE% %NULLABLE% %DEFAULT%";
        return $this;
    }
    public function char($name, $length = 100)
    {
        $this->query[] = "`{$name}` CHAR($length) %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function date($name)
    {
        $this->query[] = "`{$name}` DATE %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function dateTime($name)
    {
        $this->query[] = "`{$name}` DATETIME %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function increments($id)
    {
        $this->query[] = "`{$id}` INT %SIGNED% %UNIQUE% %NULLABLE% AUTO_INCREMENT";
        return $this;
    }
    public function timestamps()
    {
        $this->query[] = "`created_at` TIMESTAMP  %NULLABLE% %DEFAULT% ";
        $this->query[] = "`updated_at` TIMESTAMP %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function timestamp($name)
    {
        $this->query[] = "`{$name}` TIMESTAMP %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function text($name)
    {
        $this->query[] = "`{$name}` TEXT %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function time($name)
    {
        $this->query[] = "`{$name}` TIME %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function softDeletes()
    {
        $this->query[] = "`deleted_at` TIMESTAMP %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function integer($name)
    {
        $this->query[] = "`{$name}` INTEGER %SIGNED% %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function tinyInteger($name)
    {
        $this->query[] = "`{$name}` TINYINT(128) %SIGNED% %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function mediumInteger($name)
    {
        $this->query[] = "`{$name}` MEDIUMINT %SIGNED% %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function smallInteger($name)
    {
        $this->query[] = "`{$name}` SMALLINT %SIGNED% %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function float($name)
    {
        $this->query[] = "`{$name}` FLOAT %SIGNED% %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function double($name, $start, $stop)
    {
        $this->query[] = "`{$name}` DOUBLE($start,$stop) %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function decimal($name, $start, $stop)
    {
        $this->query[] = "`{$name}` DECIMAL($start,$stop) %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function enum($name, $items)
    {
        if (is_array($items) and count($items) > 0) {
            $enum_items = "";
            foreach ($items as $value) {
                $enum_items .= "'$value',";
            }
            $enum_items    = substr($enum_items, 0, strlen($enum_items) - 1);
            $this->query[] = "`{$name}` enum({$enum_items}) %UNIQUE% %NULLABLE% %DEFAULT% ";
        } else {
            $this->query[] = "`{$name}` enum('{$items}') %UNIQUE% %NULLABLE% %DEFAULT% ";
        }
        return $this;
    }
    public function longText($name)
    {
        $this->query[] = "`{$name}` LONGTEXT %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function engine($name)
    {
        $this->engineValue = strtoupper($name);
        return $this;
    }
    public function charset($name)
    {
        $this->charSet = $name;
        return $this;
    }
    public function ip($name)
    {
        $this->query[] = "`{$name}` VARCHAR(20) %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function string($name, $length = 100)
    {
        $this->query[] = "`{$name}` VARCHAR($length) %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function int($name, $length = 10)
    {
        $this->query[] = "`{$name}` INT($length) %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function rememberToken()
    {
        $this->query[] = "`{remember_token}` VARCHAR(512) %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function foreign($field)
    {
        $this->query[] = "[BOTTOM]FOREIGN KEY(`{$field}`) REFERENCES `%ON%`(`%REFERENCES%`) %ONUPDATE% %ONDELETE%,";
        return $this;
    }
    public function references($field)
    {
        $end               = end($this->query);
        $data              = str_replace("%REFERENCES%", $field, $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }
    public function after($name)
    {
        $end   = trim(end($this->query));
        $start = strpos($end, " ");
        if (!$start) {
            return $this;
        }

        $str           = trim(substr($end, $start));
        $end           = strpos($str, " ");
        $type          = trim(substr($str, 0, $end));
        $this->query[] = "`{$name}` $type %UNIQUE% %NULLABLE% %DEFAULT% ";
        return $this;
    }
    public function on($table)
    {
        $end               = end($this->query);
        $data              = str_replace("%ON%", $table, $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }
    public function onDelete($option)
    {
        $upper_option = strtoupper($option);
        if ($upper_option != "RESTRICT" and $upper_option != "CASCADE" and $upper_option != "SET NULL" and $upper_option != "NO ACTION") {
            return $this;
        }

        $end               = end($this->query);
        $data              = str_replace("%ONDELETE%", "ON DELETE $upper_option", $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }
    public function onUpdate($option)
    {
        $upper_option = strtoupper($option);
        if ($upper_option != "RESTRICT" and $upper_option != "CASCADE" and $upper_option != "SET NULL" and $upper_option != "NO ACTION") {
            return $this;
        }

        $end               = end($this->query);
        $data              = str_replace("%ONUPDATE%", "ON UPDATE $upper_option", $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }
    public function primary($name)
    {
        $this->query[] = "[BOTTOM]PRIMARY KEY (`{$name}`),";
        return $this;
    }
    public function index($cols)
    {
        $indexes = $cols;
        if (is_array($cols)) {
            $indexes = implode(",", $cols);
        }
        $this->query[] = "[BOTTOM]INDEX ($indexes),";
        return $this;
    }
    public function unique()
    {
        $end               = end($this->query);
        $data              = str_replace("%UNIQUE%", "UNIQUE", $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }
    public function nullable()
    {
        $end               = end($this->query);
        $data              = str_replace("%NULLABLE%", "", $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }
    public function unsigned()
    {
        $end               = end($this->query);
        $data              = str_replace("%SIGNED%", "UNSIGNED", $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }

    public function defaultValue($value)
    {
        $end               = end($this->query);
        $data              = str_replace("%DEFAULT%", "DEFAULT '" . $value . "'", $end);
        $key               = key($this->query);
        $this->query[$key] = $data;
        return $this;
    }

    public function createQuery()
    {
        if (!count($this->query)) {
            return null;
        }

        $bottom = "";
        $top    = "";
        $query  = "CREATE TABLE IF NOT EXISTS $this->tableName (";
        foreach ($this->query as $value) {
            $q = str_replace("%DEFAULT%", '', $value);
            $q = str_replace("%ONUPDATE%", '', $q);
            $q = str_replace("%UNIQUE%", '', $q);
            $q = str_replace("%ONDELETE%", '', $q);
            $q = str_replace("%NULLABLE%", 'NOT NULL', $q);
            $q = str_replace("%SIGNED%", '', $q);
            if (substr($q, 0, 5) == "[TOP]") {
                $top .= substr($q, 5) . ',';
            } elseif (substr($q, 0, 8) == "[BOTTOM]") {
                $bottom .= substr($q, 8) . ',';
            } else {
                $query .= trim($q) . ",";
            }

        }
        $query = $top . $query . $bottom;
        $query = substr($query, 0, strlen($query) - 1);
        $query = str_replace(",,", ",", $query);
        $query = rtrim($query, ",") . ") ENGINE=$this->engineValue DEFAULT CHARSET=$this->charSet;";
        return trim($query) == "" ? null : trim($query);
    }
    public function execute($rawQuery = null)
    {
        $query = $this->createQuery();
        if ($rawQuery != null) {
            $query = $rawQuery;
        }

        $this->db->startTransaction();
        if (!$this->db->rawQuery($query)) {
            $this->db->rollback();
            return false;
        }
        $this->db->commit();
        return true;
    }
}
