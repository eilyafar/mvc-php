<?php
namespace Mini\Core;
use Mini\Core\Phpmailer;
use Mini\Core\Smtp;
class Mail 
{
	public $mail=null;
	public $to=null;
	public $to_name=null;
	public $subject='Subject';
	public $content=null;
	public $header_text='This is a Header';
	public $footer_text='This is a footer';
	function __construct()
	{
		$mail = new PHPMailer;
		//$mail->SMTPDebug = 3;                               
		if(EMAIL_USE_SMTP) $mail->isSMTP();                                      
		$mail->Host = EMAIL_SMTP_HOST;
		$mail->CharSet = EMAIL_CHARSET;
		$mail->SMTPAuth = EMAIL_SMTP_AUTH;                   
		$mail->Username = EMAIL_SMTP_USERNAME;                
		$mail->Password = EMAIL_SMTP_PASSWORD;                          
		$mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;                          
		$mail->Port = EMAIL_SMTP_PORT;                                   
		$mail->setFrom(EMAIL_FROM, EMAIL_FROM_NAME);
		$mail->isHTML(IS_EMAIL_HTML); 
		$this->mail=$mail;               	
	}
	public function sendMail()
	{
		$to_name=null;
		if($this->to_name!=null)
		{
			$to_name=$this->to_name;
		}
		if($this->to==null) return false;
		$this->mail->addAddress($this->to);
		$this->mail->Body=
		'<div style="font-family:'.EMAIL_FONTFAMILY.';border:#ccc solid 2px;padding:10px;border-radius:4px">
		<div style="border-bottom:#ccc solid 1px;padding:10px">
		'.$this->header_text.'
		</div>
		<div style="padding:10px">
		<p style="border-bottom:#333 dashed 1px;">Dear '.$to_name.'</p>
		'.$this->content.'
		</div>
		<div style="border-top:#ccc solid 1px;padding:10px">
		'.$this->footer_text.'
		</div><div class="yj6qo"></div><div class="adL">
		</div></div>';
		$this->mail->Subject = $this->subject;
		return $this->mail->send();   
	}
}