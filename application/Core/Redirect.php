<?php
namespace Mini\Core;

use Mini\Core\Session;

/*
 * Nibbleblog -
 * http://www.nibbleblog.com
 * Author Diego Najar
 * All Nibbleblog code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 */
class Redirect
{
    public static function to($location)
    {
        if (!headers_sent()) {
            header("Location:" . $location, true, 302);
            exit;
        }
        exit('<meta http-equiv="refresh" content="0; url=' . $location . '" />');
    }
    public static function refresh()
    {
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        header('Location: ' . $actual_link, true, 302);
        exit('<meta http-equiv="refresh" content="0; url=' . $location . '" />');
    }
    public static function back($var = null)
    {
        if ($var != null) {
            //if (!session_id()) @session_start();
            $session         = new Session();
            $_SESSION['var'] = $var;
        }
        if (!headers_sent()) {
            header('Location: ' . $_SERVER['HTTP_REFERER'], true, 302);
            exit;
        }
        exit('<meta http-equiv="refresh" content="0; url=' . $_SERVER['HTTP_REFERER'] . '" />');
    }
    public static function controller($base, $controller, $action, $parameters = array())
    {
        $url = '';
        foreach ($parameters as $key => $value) {
            $url .= '&' . $key . '=' . $value;
        }
        self::to(HTML_PATH_ROOT . $base . '.php?controller=' . $controller . '&action=' . $action . $url);
    }
}
