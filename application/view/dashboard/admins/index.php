<div class="content-wrapper">
<section class="content-header">
<?php
use Mini\Core\Encryption;
use Mini\Model\Admin;
$admin            = new Admin();
$fetch_admin_info = AJAX . '/fetch_admin';
$update_admin     = AJAX . '/update_admin';
$add_admin        = AJAX . '/add_admin';
$delete_admin     = AJAX . '/delete_admin';
$activation_admin     = AJAX . '/activation_admin';
echo $header;
?>
</section>


<section class="content">
<div class="row">
<div class="col-sm-12">
<div class="box box-info">
  <div class="box-header">
    <h3 class="box-title">Manage Admins</h3>
  <button class="btn btn-success pull-right" data-toggle="modal" onclick="clear_add_admin()" data-target="#add-admin-modal">+Add New Admin</button>
  </div>
  <hr>
  <div class="box-body">
  <table id="tbl-visits" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Fullname</th>
        <th>Username</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>State</th>
        <th>Actions</th>
      </tr>
      </thead>
    <tbody>                  
    <?php
$counter = 0;
foreach ($admin->getAllAdmins() as $user) {
    $edit_key = RANDOM_STRING;
    $edit_id  = Encryption::encrypt($user['id'], $edit_key);
    $counter++;
    $delete_key = RANDOM_STRING;
    $delete_id  = Encryption::encrypt($user['id'], $delete_key);
    $activation_key = RANDOM_STRING;
    $activation_id  = Encryption::encrypt($user['id'], $activation_key);
    $activation='<button type="button" onclick="activation_admin(\''.$activation_id.'\',\''.$activation_key.'\',\''.$activation_admin.'\')" 
    class="btn btn-warning">Deactive</button>';
    $activation=$user['state']==0?'<button type="button" 
    onclick="activation_admin(\''.$activation_id.'\',\''.$activation_key.'\',\''.$activation_admin.'\')" 
    class="btn btn-success">Active</button>':$activation;
    echo "<tr >
            <td>{$user['first_name']} {$user['last_name']}</td>
            <td>{$user['username']}</td>
            <td>{$user['email']}</td>
            <td>{$user['mobile']}</td>
            <td>{$user['last_name']}</td>
            <td  >
            <div id='td-" . $counter . "' class='btn-group' style='text-align: right;'>";
    echo '<button type="button" class="btn btn-info"
            data-toggle="modal" onClick=fetch_admin_info("' . $edit_id . '","' . $edit_key . '","' . $fetch_admin_info . '") data-target="#update-admin-modal">Edit</button>
            <button type="button" role="button" class="btn btn-danger"
            onClick=delete_admin(\'' . $delete_id . '\',\'' . $delete_key . '\',\'' . $delete_admin . '\',\'' . $counter . '\');  
            data-trigger="focus" >Trah</button>'.$activation.'</td>
            </tr>"';}
?>
                     </tbody>
                    <tfoot>
                      <tr>
                        <th>Fullname</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>State</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
          </div>
        </div>
      </div>
</div>
</div>
</section>




<div class="modal fade" id="update-admin-modal" tabindex="-1" role="dialog" aria-labelledby="update-admin-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="update-admin-modal-title">...</h4>
      </div>
      <div class="modal-body" style="display: none;" id="body-update-admin">
      <form class="form-horizontal" role="form" id="frm-update" action="<?php echo DASHBOARD; ?>/profile/update" method="post">
                <input type="hidden" name="id" value="0" id="id">
                <input type="hidden" name="key" value="0" id="key">
                <input type="hidden" name="csrf_token" value="0" id="csrf_token">
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="firstName" name="firstname"  placeholder="First Name"
                        value="" maxlength="32" class="form-control" autofocus required="">
                        <input type="text" id="lastName" name="lastname" maxlength="32" placeholder="Last Name"
                        value="" class="form-control" style="margin-top: 5px;" required="">
                        <span class="help-block">Enter First Name and Last Name,  eg.: Vahid, Mahmoudian</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" maxlength="128" placeholder="Email" class="form-control"
                        value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" id="username" name="username" maxlength="64" placeholder="Username" class="form-control"
                        value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" id="password" name="password" maxlength="64" placeholder="New Password" class="form-control"
                        value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="col-sm-3 control-label">Mobile.No</label>
                    <div class="col-sm-9">
                        <input type="text" id="mobile" name="mobile" placeholder="Mobile" maxlength="11" class="form-control"
                        value="">
                        <span class="help-block">Enter Mobile Number,  eg.: 09121234567</span>

                    </div>
                    </div>
                    </form>
                    <p class="alert output" style="display: none;"></p>
                    </div>


                   <center>
                  <div class="overlay">
                        <i style="font-size: 20px;" class="fa fa-refresh fa-spin"></i>
                  </div>
                </center>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" disabled="disabled" onclick="update_admin('<?php echo $update_admin; ?>')" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="add-admin-modal" tabindex="-1" role="dialog" aria-labelledby="add-admin-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="update-admin-modal-title">Add new admin</h4>
      </div>
      <div class="modal-body"  id="add-admin-body">
      <form class="form-horizontal" role="form" id="frm-add" action="<?php echo DASHBOARD; ?>/profile/update" method="post">
                <input type="hidden" name="new_id" value="0" id="new-id">
                <input type="hidden" name="new_key" value="0" id="new-key">
                <input type="hidden" name="new_csrf_token" value="0" id="new-csrf-token">
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="new-firstName" name="new_firstname"  placeholder="First Name"
                        value="" maxlength="32" class="form-control" autofocus required="">
                        <input type="text" id="new-lastName" name="new_lastname" maxlength="32" placeholder="Last Name"
                        value="" class="form-control" style="margin-top: 5px;" required="">
                        <span class="help-block">Enter First Name and Last Name,  eg.: Vahid, Mahmoudian</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="new-email" name="new_email" maxlength="128" placeholder="Email" class="form-control"
                        value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" id="new-username" name="new_username" maxlength="64" placeholder="Username" class="form-control"
                        value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" id="new-password" name="new_password" maxlength="64" placeholder="New Password" class="form-control"
                        value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="col-sm-3 control-label">Mobile.No</label>
                    <div class="col-sm-9">
                        <input type="text" id="new-mobile" name="new_mobile" placeholder="Mobile" maxlength="11" class="form-control"
                        value="">
                        <span class="help-block">Enter Mobile Number,  eg.: 09121234567</span>

                    </div>
                    </div>
                    </form>
                    <p class="alert output" style="display: none;"></p>
                    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="add_admin('<?php echo $add_admin; ?>')" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
  </div>


  </div>
  </div>