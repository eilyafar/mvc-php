  <div class="content-wrapper">
    <section class="content-header">
      <?php
use Mini\Core\FlashMessages;
use Mini\Model\Admin;
$admin = new Admin();
echo $header;
?>
    </section>
    <section class="content">
        <div class="row">
                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Update Profile</h3>
                          <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
            <form class="form-horizontal" role="form" id="frm-update" action="<?php echo DASHBOARD; ?>/profile/update" method="post">
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="firstName" name="firstname"  placeholder="First Name"
                        value="<?php echo $admin->firstName(); ?>" maxlength="32" class="form-control" autofocus required="">
                        <input type="text" id="lastName" name="lastname" maxlength="32" placeholder="Last Name"
                        value="<?php echo $admin->lastName(); ?>" class="form-control" style="margin-top: 5px;" required="">
                        <span class="help-block">Enter First Name and Last Name,  eg.: Vahid, Mahmoudian</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" maxlength="128" placeholder="Email" class="form-control"
                        value="<?php echo $admin->email(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" id="username" name="username" maxlength="64" placeholder="Username" class="form-control"
                        value="<?php echo $admin->username(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="col-sm-3 control-label">Mobile.No</label>
                    <div class="col-sm-9">
                        <input type="text" id="mobile" name="mobile" placeholder="Mobile" maxlength="11" class="form-control"
                        value="<?php echo $admin->mobile(); ?>">
                        <span class="help-block">Enter Mobile Number,  eg.: 09121234567</span>

                    </div>
                </div>
                        <div class="col-sm-12">
                                <?php
$message = new FlashMessages();
if ($message->hasMessages()) {
    echo "<hr>";
    $message->display();
    echo "<hr>";
}
?>
                        </div>

                        <div id="password-confirm-modal" class="modal fade bs-confirm-password-modal-sm" tabindex="-1" role="dialog">
                          <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="gridSystemModalLabel">Confirm</h4>
                                </div>
                                <div class="modal-body">
                                <p>Enter your password to continue</p>
                                <input type="password" id="inp-password" name="password" placeholder="Your password" maxlength="64" class="form-control"
                                        value="">
                                <p style="display: none;margin-top: 5px;" class="alert alert-dismissible" id="check-password-output">

                                </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="button" id="btn-change-password" onclick="checkPassword('<?php echo URL .
    "dashboard/ajax/confirm"; ?>')"
                                     class="btn btn-primary"  value="Continue" />
                              </div>
                            </div>
                          </div>
                        </div>


                    <div class="col-sm-4 col-sm-offset-8">

                        <div class="col-sm-6">
                        <a href="#change-password" data-toggle="modal"
                        data-target=".bs-change-password-modal-sm" class="btn btn-success btn-block">Change Password</a>
                        </div>
                        <div class="col-sm-6">
                        <a href="#change-password" data-toggle="modal"
                        data-target=".bs-confirm-password-modal-sm" class="btn btn-primary btn-block">Update</a>
                        </div>
                    </div>
            </form> <!-- /form -->
            <div id="password-change-modal" class="modal fade bs-change-password-modal-sm" tabindex="-1" role="dialog">
                          <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <form class="form-horizontal" role="form" id="frm-change-password" method="post">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="gridSystemModalLabel">Change Password</h4>
                                </div>
                                <div class="modal-body">
                                <p>Enter current and new passwords</p>
                                <input type="password" id="inp-old-password" name="old_password" placeholder="Current Password"
                                maxlength="64" style="margin-top: 4px;" class="form-control" value="">


                                 <input type="password" id="inp-new-password" name="new_password" placeholder="New Password"
                                maxlength="64" style="margin-top: 4px;" class="form-control" value="">
                                <input type="password" id="inp-confirm-password" name="confirm_password" placeholder="Confirm Password"
                                maxlength="64" style="margin-top: 8px;" class="form-control" value="">
                                <p style="display: none;margin-top: 5px;" class="alert alert-dismissible" id="change-password-output">

                                </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="button" id="btn-change-password"
                                    onclick="changePassword('<?php echo URL . "dashboard/ajax/changepassword"; ?>')"
                                     class="btn btn-primary"  value="Change" />
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
