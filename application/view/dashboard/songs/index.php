  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <?php
      echo isset($header)?$header:'';
      ?>
    </section>
    <section class="content" >
        <div class="row">
                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Links</h3>
                          <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body">     
                            <h3>Add a song</h3>
                            <form action="<?php echo URL; ?>songs/addsong" method="POST">
                                <label>Artist</label>
                                <input type="text" name="artist" value="" required />
                                <label>Track</label>
                                <input type="text" name="track" value="" required />
                                <label>Link</label>
                                <input type="text" name="link" value="" />
                                <input type="submit" name="submit_add_song" value="Submit" />
                            </form>
                        </div>
                    </div>
                </div>
        </div>


        <div class="row">
                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Links</h3>
                          <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body">     
                        <h3>Amount of songs: <?php echo $amount_of_songs; ?></h3>
                        <h3>Amount of songs (via AJAX)</h3>
                        <div id="javascript-ajax-result-box"></div>
                        <div>
                            <button id="javascript-ajax-button">Click here to get the amount of songs via Ajax (will be displayed in #javascript-ajax-result-box ABOVE)</button>
                        </div>
                        </div>
                    </div>
                </div>
        </div>


        <div class="row">
                <div class="col-sm-12">

                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Links</h3>
                          <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body">     
                                   <table class="table table-hover">
                                    <thead style="background-color: #ddd; font-weight: bold;">
                                    <tr>
                                        <th>Id</th>
                                        <th>Artist</th>
                                        <th>Track</th>
                                        <th>Link</th>
                                        <th>DELETE</th>
                                        <th>EDIT</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($songs as $song) { ?>
                                        <tr>
                               
                                            <td><?php if (isset($song['id'])) echo htmlspecialchars($song['id'], ENT_QUOTES, 'UTF-8'); ?></td>
                                            <td><?php if (isset($song['artist'])) echo htmlspecialchars($song['artist'], ENT_QUOTES, 'UTF-8'); ?></td>
                                            <td><?php if (isset($song['track'])) echo htmlspecialchars($song['track'], ENT_QUOTES, 'UTF-8'); ?></td>
                                            <td>
                                                <?php if (isset($song->link)) { ?>
                                                    <a href="<?php echo htmlspecialchars($song->link, ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlspecialchars($song->link, ENT_QUOTES, 'UTF-8'); ?></a>
                                                <?php } ?>
                                            </td>
                                            <td><a href="<?php echo URL . 'songs/deletesong/' . htmlspecialchars($song['id'], ENT_QUOTES, 'UTF-8'); ?>">delete</a></td>
                                            <td><a href="<?php echo URL . 'songs/editsong/' . htmlspecialchars($song['id'], ENT_QUOTES, 'UTF-8'); ?>">edit</a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
        </div>

                    </div>
                </div>
        </div>

    

