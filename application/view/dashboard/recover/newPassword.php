<?php
use Mini\Core\FlashMessages;
use Mini\Core\Template;
?>
  <div class="login-box-body">
  <?php if (isset($password_change) and $password_change): ?>
    <p class="text-success"><strong>Successful</strong><br>Your password has been changed successfully!</p>
    <br><br><a href="<?php echo LOGIN; ?>">Login</a>
  <?php else: ?>
    <p class="login-box-msg" style="text-align: left;">Enter new password</p>
    <form action="<?php echo RECOVER; ?>/store" method="post">
      <div class="form-group has-feedback">
        <input type="hidden" name="recover_token" value="<?php echo $recover_token; ?>">
        <input type="hidden" name="email" value="<?php echo isset($email_address) ? $email_address : ''; ?>">
        <input type="password" class="form-control" placeholder="New Password"
        name="password" maxlength="32" required style="border-radius: 4px;">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Confirm New Password"
        name="confirm_password" maxlength="32" required style="border-radius: 4px;">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <?php

echo Template::csrf();
?>
      <div class="row">
        <div class="col-xs-6">
        <a href="<?php echo LOGIN; ?>">Back to login</a><br>
        </div>
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block ">Change password</button>
        </div>
      </div>
      <br>
      <div class="row">
        <?php
$message = new FlashMessages();
if ($message->hasMessages()) {
    $message->display();
}
?>
      </div>
    </form>
  <?php endif;?>
  </div>
