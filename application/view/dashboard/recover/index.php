<div class="login-box-body">
<?php  
   use Mini\Core\Template;
   use Mini\Core\FlashMessages;
   if(isset($_SESSION, $_SESSION['password_change']) and $_SESSION['password_change']):
   $_SESSION=array();
?>
<p class="text-success"><strong>Successful</strong><br>Your password has been changed successfully!</p>
<br>
<hr>
<br><a href="<?php echo LOGIN; ?>">Login</a>
<?php else: ?>
<p class="login-box-msg" style="text-align: left;">Enter your email address and we'll help you reset your password</p>
<form id="recover" action="<?php echo URL; ?>dashboard/checkemail" method="post">
   <div class="form-group has-feedback">
      <input type="email" id="email" class="form-control" placeholder="Registered Email Address"
         name="email" maxlength="32" required style="border-radius: 4px;">
      <span class="fa fa-envelope-o form-control-feedback"></span>
   </div>
   <?php 
      echo Template::csrf();
      ?>
   <div class="row">
      <div class="col-xs-6">
         <a href="<?php echo LOGIN; ?>">Back to login</a><br> 
      </div>
      <div class="col-xs-6">
         <button type="submit" name="login" class="btn btn-primary btn-block ">Check Email</button>
      </div>
   </div>
   <br>
   <div class="row">
      <?php
         $message=new FlashMessages();
         if ($message->hasMessages()) 
         {
             $message->display();
         }
         ?>
   </div>
</form>
<?php endif ?>
</div>