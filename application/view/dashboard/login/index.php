<?php
   use Mini\Core\Template;
   use Mini\Core\FlashMessages;
   use Mini\Core\Session;
$session=new Session();
   var_dump($_SESSION);
?>
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="<?php echo URL; ?>dashboard/checklogin" method="post">
      <div class="form-group has-feedback">
        <input type="hidden" name="login">
        <input type="text" class="form-control" placeholder="Username" 
        value="<?php echo isset($_COOKIE['username'])?$_COOKIE['username']:''; ?>" 
        name="username" maxlength="32" required style="border-radius: 4px;">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" 
        name="password" maxlength="32" required style="border-radius: 4px;">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <?php 
      echo Template::csrf();
      if(CAPTCHA): ?>
      <div class="row">
        <div class="col-xs-10">
          <img id='img-captcha' src="<?php echo URL; ?>dashboard/login/captch">
        </div>
        <div class="col-xs-2">
          <a href="#refresh" style="margin-top: 10px;font-size: 20px;display: inline-block;" onclick="refresh()">
          <i class="fa fa-refresh" aria-hidden="true"></i></a>
        </div>
        <!-- /.col --> 
      </div>
        <div class="form-group has-feedback">
        <input type="hidden" name="login">
        <input type="text" maxlength="<?php echo CAPTCHA_CODE_LENGTH; ?>" name="captcha_code" class="form-control" placeholder="Captcha" 
        name="username" maxlength="32" required style="border-radius: 4px;">
        <span class="fa fa-low-vision  form-control-feedback"></span>
      </div>
      <?php
      endif;
      ?>
      <div class="row">
        <div class="col-xs-8">
          <label>
          <input id="rememberme" name="rememberme" value="" type="checkbox"
          <?php echo isset($_COOKIE['rememberme'])?'checked="checked"':''; ?> />
          &nbsp;Remember me</label>   
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="login" class="btn btn-primary btn-block ">Sign In</button>
        </div>
      </div>
      <br>
      <div class="row">
        <?php
        $message=new FlashMessages();
        if ($message->hasMessages()) 
        {
            $message->display();
        }
        ?>
      </div>
    </form>
    <a href="<?php echo RECOVER; ?>">I forgot my password</a><br>
  </div>
</div>