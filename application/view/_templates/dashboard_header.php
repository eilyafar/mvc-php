<?php
use Mini\Model\Admin;
$admin = new Admin();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($title) ? $title : 'Dashboard'; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo URL; ?>css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo URL; ?>css/font-awesome.css">
  <!-- Ionicons -->
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
 -->  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL; ?>css/admin-lte.css">
  <link rel="stylesheet" href="<?php echo URL; ?>css/all-skins.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="<?php echo URL; ?>js/jquery-2.2.3.min.js"></script>
  <?php
if (isset($scripts)) {
    foreach ($scripts as $key => $script) {
        if ($key == "css") {
            foreach ($script as $style) {
                echo "<link rel='stylesheet' href='" . URL . "css/$style.css' />\r\n";
            }
        }
        if ($key == "js") {
            foreach ($script as $js) {
                echo "<script src='" . URL . "js/$js.js'></script>\r\n";
            }
        }
    }
}
;?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo strtoupper(substr(APP_NAME,0,1).substr(APP_NAME,-1)); ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?php echo APP_NAME; ?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" id="nnk" class="dropdown-toggle" data-toggle="dropdown"
            data-container="body" data-toggle="popover" data-placement="bottom"
            data-content="Vivamussagittis lacus vel augue laoreet rutrum faucibus.">
              <span class="hidden-xs"><?php echo $admin->firstName(); ?></span>
              <span class="visible-xs"><?php echo $admin->shortName(); ?></span>
            </a>
            <div class="col-md-12 dropdown-menu"  style="border: #367FA9 solid 1px;border-radius: 4px;
            border-top-left-radius: 0;border-top-right-radius: 0;" >
          <!-- Widget: user widget style 1 -->
          <div class="box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header" style="background-color: #367FAA;border-radius: 0;">
              <h3 class="widget-user-username" style="color: #fff;" ><?php echo $admin->fullName(); ?></h3>
              <h5 class="widget-user-desc" style="color: #ddd;"><?php echo $admin->username(); ?></h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a class="text-black" href="<?php echo URL; ?>dashboard/profile">My Profile</a></li>
                <!-- <li><a class="text-black" href="<?php echo URL; ?>dashboard/messages">Messages
                <span class="pull-right badge bg-blue">0</span></a></li> -->
                <li><a class="text-black" href="<?php echo URL; ?>dashboard/admins">Admins</a></li>
                <li><a class="text-black" href="<?php echo URL; ?>dashboard/roles">Roles</a></li>
                <li><a class="text-black" href="<?php echo URL; ?>dashboard/logout">Logout</a></li>



              </ul>
            </div>
          </div>
        </div>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <?php
include 'dashboard_sidebar.php';
?>
</aside>


