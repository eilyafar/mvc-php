    </div>
    </div>
    <footer class="footer">
      <div class="container">
        <p class="text-muted"><?php echo FOOTER_TEXT; ?></p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo URL; ?>js/jquery-2.2.3.min.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap.min.js"></script>
  </body>
</html>