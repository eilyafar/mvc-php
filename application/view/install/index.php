<?php
use Mini\Core\FlashMessages;
use Mini\Core\Session;
echo isset($header) ? $header : '';
$set_request = false;
$requests=[];
$session = new Session();
if (isset($_SESSION['var'])) {
    $set_request = true;
    $requests    = $_SESSION['var'];
    unset($_SESSION['var']);
}
echo "abc";
$message = new FlashMessages();
if ($message->hasMessages()) {
    $message->display();
    echo "<hr>";
}
if (!$isInstalled):
?>
<div class="col-sm-7" style="border-right: #ccc dashed 1px;padding-right: 20px;">
            <br>
              <div class="row">

              </div>
            <form class="form-horizontal" role="form" action="<?php echo INSTALL; ?>/register" method="post">
                <h2>Register as administrator</h2>
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="firstName" name="firstname"  placeholder="First Name"
                        value="<?php echo $set_request ? $requests['firstname'] : ''; ?>" maxlength="32" class="form-control" autofocus required="">
                        <input type="text" id="lastName" name="lastname" maxlength="32" placeholder="Last Name"
                        value="<?php echo $set_request ? $requests['lastname'] : ''; ?>" class="form-control" style="margin-top: 5px;" required="">
                        <span class="help-block">Enter First Name and Last Name,  eg.: Vahid, Mahmoudian</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" maxlength="128" placeholder="Email" class="form-control"
                        value="<?php echo $set_request ? $requests['email'] : ''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" id="username" name="username" maxlength="64" placeholder="Username" class="form-control"
                        value="<?php echo $set_request ? $requests['username'] : ''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" id="password" name="password" maxlength="64" placeholder="Password" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="col-sm-3 control-label">Mobile.No</label>
                    <div class="col-sm-9">
                        <input type="text" id="mobile" name="mobile" placeholder="Mobile" maxlength="11" class="form-control"
                        value="<?php echo $set_request ? $requests['mobile'] : ''; ?>">
                        <span class="help-block">Enter Mobile Number,  eg.: 09121234567</span>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="agreement">I have read and understand this <a href="#" data-toggle="modal"
                                data-target=".bs-example-modal-lg">agreement</a>
                                , and i accept and agree to all of its terms and conditions.
                            </label>
                        </div>
                    </div>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                    </div>
                </div>
            </form> <!-- /form -->
        </div> <!-- ./col-sm-8 -->
        <div class="col-sm-5" >
		<center><img class="img-responsive" src="<?php echo URL; ?>images/installation.png"></center>
		</div>
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Agreement</h4>
                </div>
                <div class="modal-body">
                <?php echo AGREEMENT; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
<?php
else:
?>
<div class="col-sm-7">
<h3 class="text-info"><strong>Hurrah!</strong> The <?php echo APP_NAME; ?> has been successfully completed.</h3>
<a href="<?php echo DASHBOARD; ?>/login">Login</a>
</div>
<div class="col-sm-5" >
<center><img class="img-responsive" src="<?php echo URL; ?>images/hurrah.png"></center>
</div>
<?php
endif;
?>