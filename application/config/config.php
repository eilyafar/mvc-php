<?php

/**
 * Configuration
 *
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 */

/**
 * Configuration for: Error reporting
 * Useful to show every little problem during development, but only show hard errors in production
 */
define('ENVIRONMENT', 'development');

if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

/**
 * Configuration for: Login
 */
define('APP_NAME', 'Freamwork'); //APP Name
define('LOGIN_EXPIRY_TIME', 2000); //30MIN
define('PAST_TIME', time() - 100); //NOW-100SEC
define('REMEMBERME_EXPIRY_TIME', time() + 31536000); //1Year
define('SECURITY_KEY', 'RdrHmHCQwH');
define('LOGIN_MULTIPLE_USERS', true);
define('LOGIN_MULTIPLE_ADMINS', true);
define('APP_VERSION', '1.0.0');
/**
 * Configuration for: URL
 * Here we auto-detect your applications URL and the potential sub-folder. Works perfectly on most servers and in local
 * development environments (like WAMP, MAMP, etc.). Don't touch this unless you know what you do.
 *
 * URL_PUBLIC_FOLDER:
 * The folder that is visible to public, users will only have access to that folder so nobody can have a look into
 * '/application' or other folder inside your application or call any other .php file than index.php inside '/public'.
 *
 * URL_PROTOCOL:
 * The protocol. Don't change unless you know exactly what you do. This defines the protocol part of the URL, in older
 * versions of MINI it was 'http://' for normal HTTP and 'https://' if you have a HTTPS site for sure. Now the
 * protocol-independent '//' is used, which auto-recognized the protocol.
 *
 * URL_DOMAIN:
 * The domain. Don't change unless you know exactly what you do.
 * If your project runs with http and https, change to '//'
 *
 * URL_SUB_FOLDER:
 * The sub-folder. Leave it like it is, even if you don't use a sub-folder (then this will be just '/').
 *
 * URL:
 * The final, auto-detected URL (build via the segments above). If you don't want to use auto-detection,
 * then replace this line with full URL (and sub-folder) and a trailing slash.
 */

define('FOOTER_TEXT', 'This is a footer text.');
define('LOGO_TEXT', '<b>Admin</b>LTE');
define('JUST_DASHBOARD', true);
define('SESSION_TIMEOUT', 3600 * 24);
define('SESSION_SALT', 'cH!swe!retReGu7W6bEDRup7usuDUh9THeD2CHeGE*ewr4n39=E@rAsp7c-Ph@pH');
define('ENCRYPTION_SALT', 'cH!swe!retReGu7W6bEDRup7usuDUh9THeD2CHeGE*ewr4n39=E@rAsp7c-Ph@pH');

define('CSRF', true);
define('CSRF_TOKEN', csrfToken(rand(10, 20)));
define('RECOVER_TOKEN', csrfToken(rand(30, 60)));
define('RECOVER_EXPIRATION', 3600 * 24);
define('RANDOM_STRING', randomString());

define('AGREEMENT', "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu f");

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
define('HOME', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER . 'home');
define('DASHBOARD', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER . 'dashboard');
define('RECOVER', DASHBOARD . '/recover');
define('LOGIN', DASHBOARD . '/login');
define('ADMINS', DASHBOARD . '/admins');
define('AJAX', DASHBOARD . '/ajax');
define('INSTALL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER . 'install');
define('LOCAL_IP_ADDRESS', '127.0.0.1');
define('IP_ADDRESS', getIpAddress());

define('TIMEZONE', 'Asia/Tehran');
define('DASHBOARD_FOOTER_TEXT', '<strong>Copyright &copy; 2016 <a href="http://vhdm.ir">Vahid Mahmoudian</a>.</strong> All rights reserved.');

/**
 * Configuration for: Database
 * This is the place where you define your database credentials, database type etc.
 */
define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'mvc');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_PORT', null);
define('DB_CHARSET', 'utf8');

/**
 * Configuration for: Agent Detection
 */
define('BROWSER', parseUserAgent('browser'));
define('PLATFORM', parseUserAgent('platform'));
define('BROWSER_VERSION', parseUserAgent('version'));

/**
 * Configuration for: password reset email data
 * Set the absolute URL to password_reset.php, necessary for email password reset links
 */
define('EMAIL_PASSWORDRESET_URL', 'http://127.0.0.1/password/reset');
define('EMAIL_PASSWORDRESET_FROM', 'no-reply@example.com');
define('EMAIL_PASSWORDRESET_FROM_NAME', 'My Project');
define('EMAIL_PASSWORDRESET_SUBJECT', 'Password reset for PROJECT XY');
define('EMAIL_PASSWORDRESET_CONTENT', 'Please click on this link to reset your password:');
/**
 * Configuration for: verification email data
 * Set the absolute URL to register.php, necessary for email verification links
 */
define('EMAIL_VERIFICATION_URL', 'http://127.0.0.1/php-login-advanced/register.php');
define('EMAIL_VERIFICATION_FROM', 'no-reply@example.com');
define('EMAIL_VERIFICATION_FROM_NAME', 'My Project');
define('EMAIL_VERIFICATION_SUBJECT', 'Account activation for PROJECT XY');
define('EMAIL_VERIFICATION_CONTENT', 'Please click on this link to activate your account:');

define('EMAIL_FROM', 'no-reply@vhdm.ir');
define('EMAIL_FROM_NAME', APP_NAME);
define('IS_EMAIL_HTML', true);
define('EMAIL_CHARSET', 'utf-8');
define('EMAIL_FONTFAMILY', 'verdana');

define("EMAIL_USE_SMTP", true);
define("EMAIL_SMTP_HOST", "smtp.gmail.com");
define("EMAIL_SMTP_AUTH", true);
define("EMAIL_SMTP_USERNAME", "vhdm.coc@gmail.com");
define("EMAIL_SMTP_PASSWORD", "2939949493");
define("EMAIL_SMTP_PORT", 465);
define("EMAIL_SMTP_ENCRYPTION", "ssl");

/**
 * Configuration for: Captcha
 */
define('CAPTCHA', false);
define('CAPTCHA_HEIGHT', 60);
define('CAPTCHA_WIDTH', 160);
define('CAPTCHA_BG_COLOR', '#FFFFFF');
define('CAPTCHA_TEXT_COLOR', '#2D89EF');
define('CAPTCHA_LINE_COLOR', '#707070');
define('CAPTCHA_NOISE_COLOR', '#707070');
define('CAPTCHA_TRANSPARENCY', 20);
define('CAPTCHA_TEXT_TRANSPARENT', true);
define('CAPTCHA_CODE_LENGTH', 6);
define('CAPTCHA_CASE_SENSITIVE', false);
define('CAPTCHA_CHARSET', 'ABCDEFGHKLMNPRSTUVWYZabcdefghklmnprstuvwyz23456789');
define('CAPTCHA_EXPIRY_TIME', 900);
define('CAPTCHA_NUM_LINES', 0);
define('CAPTCHA_NOISE_LEVEL', 0);

function csrfToken($length = 10)
{
    if (!CSRF) {
        return null;
    }

    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    $result = str_replace(" ", "", $result);
    return $result;
}

function randomString($length = 30)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function getIpAddress()
{
    $ip = LOCAL_IP_ADDRESS;
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip == '::1' ? LOCAL_IP_ADDRESS : $ip;
}

function parseUserAgent($what = 'array', $u_agent = null)
{
    if (is_null($u_agent)) {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $u_agent = $_SERVER['HTTP_USER_AGENT'];
        } else {
            throw new \InvalidArgumentException('parse_user_agent requires a user agent');
        }
    }
    $platform = null;
    $browser  = null;
    $version  = null;
    $empty    = array('platform' => $platform, 'browser' => $browser, 'version' => $version);
    if (!$u_agent) {
        return $empty;
    }

    if (preg_match('/\((.*?)\)/im', $u_agent, $parent_matches)) {
        preg_match_all('/(?P<platform>BB\d+;|Android|CrOS|Tizen|iPhone|iPad|iPod|Linux|Macintosh|Windows(\ Phone)?|Silk|linux-gnu|BlackBerry|PlayBook|X11|(New\ )?Nintendo\ (WiiU?|3?DS)|Xbox(\ One)?)
				(?:\ [^;]*)?
				(?:;|$)/imx', $parent_matches[1], $result, PREG_PATTERN_ORDER);
        $priority           = array('Xbox One', 'Xbox', 'Windows Phone', 'Tizen', 'Android', 'CrOS', 'X11');
        $result['platform'] = array_unique($result['platform']);
        if (count($result['platform']) > 1) {
            if ($keys = array_intersect($priority, $result['platform'])) {
                $platform = reset($keys);
            } else {
                $platform = $result['platform'][0];
            }
        } elseif (isset($result['platform'][0])) {
            $platform = $result['platform'][0];
        }
    }
    if ($platform == 'linux-gnu' || $platform == 'X11') {
        $platform = 'Linux';
    } elseif ($platform == 'CrOS') {
        $platform = 'Chrome OS';
    }
    preg_match_all('%(?P<browser>Camino|Kindle(\ Fire)?|Firefox|Iceweasel|Safari|MSIE|Trident|AppleWebKit|TizenBrowser|Chrome|
				Vivaldi|IEMobile|Opera|OPR|Silk|Midori|Edge|CriOS|UCBrowser|
				Baiduspider|Googlebot|YandexBot|bingbot|Lynx|Version|Wget|curl|
				Valve\ Steam\ Tenfoot|
				NintendoBrowser|PLAYSTATION\ (\d|Vita)+)
				(?:\)?;?)
				(?:(?:[:/ ])(?P<version>[0-9A-Z.]+)|/(?:[A-Z]*))%ix',
        $u_agent, $result, PREG_PATTERN_ORDER);
    // If nothing matched, return null (to avoid undefined index errors)
    if (!isset($result['browser'][0]) || !isset($result['version'][0])) {
        if (preg_match('%^(?!Mozilla)(?P<browser>[A-Z0-9\-]+)(/(?P<version>[0-9A-Z.]+))?%ix', $u_agent, $result)) {
            return array('platform' => $platform ?: null, 'browser' => $result['browser'], 'version' => isset($result['version']) ? $result['version'] ?: null: null);
        }
        return $empty;
    }
    if (preg_match('/rv:(?P<version>[0-9A-Z.]+)/si', $u_agent, $rv_result)) {
        $rv_result = $rv_result['version'];
    }
    $browser      = $result['browser'][0];
    $version      = $result['version'][0];
    $lowerBrowser = array_map('strtolower', $result['browser']);
    $find         = function ($search, &$key, &$value = null) use ($lowerBrowser) {
        $search = (array) $search;
        foreach ($search as $val) {
            $xkey = array_search(strtolower($val), $lowerBrowser);
            if ($xkey !== false) {
                $value = $val;
                $key   = $xkey;
                return true;
            }
        }
        return false;
    };
    $key = 0;
    $val = '';
    if ($browser == 'Iceweasel') {
        $browser = 'Firefox';
    } elseif ($find('Playstation Vita', $key)) {
        $platform = 'PlayStation Vita';
        $browser  = 'Browser';
    } elseif ($find(array('Kindle Fire', 'Silk'), $key, $val)) {
        $browser  = $val == 'Silk' ? 'Silk' : 'Kindle';
        $platform = 'Kindle Fire';
        if (!($version = $result['version'][$key]) || !is_numeric($version[0])) {
            $version = $result['version'][array_search('Version', $result['browser'])];
        }
    } elseif ($find('NintendoBrowser', $key) || $platform == 'Nintendo 3DS') {
        $browser = 'NintendoBrowser';
        $version = $result['version'][$key];
    } elseif ($find('Kindle', $key, $platform)) {
        $browser = $result['browser'][$key];
        $version = $result['version'][$key];
    } elseif ($find('OPR', $key)) {
        $browser = 'Opera Next';
        $version = $result['version'][$key];
    } elseif ($find('Opera', $key, $browser)) {
        $find('Version', $key);
        $version = $result['version'][$key];
    } elseif ($find(array('IEMobile', 'Edge', 'Midori', 'Vivaldi', 'Valve Steam Tenfoot', 'Chrome'), $key, $browser)) {
        $version = $result['version'][$key];
    } elseif ($browser == 'MSIE' || ($rv_result && $find('Trident', $key))) {
        $browser = 'MSIE';
        $version = $rv_result ?: $result['version'][$key];
    } elseif ($find('UCBrowser', $key)) {
        $browser = 'UC Browser';
        $version = $result['version'][$key];
    } elseif ($find('CriOS', $key)) {
        $browser = 'Chrome';
        $version = $result['version'][$key];
    } elseif ($browser == 'AppleWebKit') {
        if ($platform == 'Android' && !($key = 0)) {
            $browser = 'Android Browser';
        } elseif (strpos($platform, 'BB') === 0) {
            $browser  = 'BlackBerry Browser';
            $platform = 'BlackBerry';
        } elseif ($platform == 'BlackBerry' || $platform == 'PlayBook') {
            $browser = 'BlackBerry Browser';
        } else {
            $find('Safari', $key, $browser) || $find('TizenBrowser', $key, $browser);
        }
        $find('Version', $key);
        $version = $result['version'][$key];
    } elseif ($pKey = preg_grep('/playstation \d/i', array_map('strtolower', $result['browser']))) {
        $pKey     = reset($pKey);
        $platform = 'PlayStation ' . preg_replace('/[^\d]/i', '', $pKey);
        $browser  = 'NetFront';
    }
    if ($what == 'platform') {
        return $platform ?: null;
    } elseif ($what == 'browser') {
        return $browser ?: null;
    } elseif ($what == 'version') {
        return $version ?: null;
    }
    return array('platform' => $platform ?: null, 'browser' => $browser ?: null, 'version' => $version ?: null);
}
