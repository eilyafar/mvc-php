<?php
namespace Mini\Model;

use Mini\Core\DateTime;
use Mini\Core\Flashmessages;
use Mini\Core\GUMP;
use Mini\Core\Mail;
use Mini\Core\Model;
use Mini\Core\Redirect;
use Mini\Core\Schema;
use pdo;
/**
 * Class for create tables
 */
class Install extends Model
{
    private static $tables = ['users', 'logs', 'admins', 'sessions'];

    public static function isInstalled()
    {

        $db           = new Model();
        $tables_count = 0;
        foreach (self::$tables as $table) {
            if (Schema::tableExists($table)) {
                $tables_count++;
            }

        }
        if (count(self::$tables) == $tables_count) {
            $count = $db->getValue("admins", "count(*)");
            if ($count > 0) {
                return true;
            }
        }
        return false;
    }
    public static function buildTables()
    {
        $tables_count = 0;
        $admins       = new Schema('admins');
        $admins->increments('id')->unsigned();
        $admins->string('first_name');
        $admins->string('last_name');
        $admins->string('username')->unique();
        $admins->string('password', 128);
        $admins->string('mobile', 20)->unique();
        $admins->string('recover_token', 128)->nullable();
        $admins->timestamp('recover_token_at')->nullable();
        $admins->string('email', 200)->unique();
        $admins->string('active_session', 128)->nullable();
        $admins->tinyInteger('access_level');
        $admins->tinyInteger('state');
        $admins->ip('last_login_ip')->nullable();
        $admins->timestamp('last_login_at')->nullable();
        $admins->timestamps()->nullable();
        $admins->softDeletes()->nullable();
        $admins->primary('id');
        $admins->execute();

        $users = new Schema('users');
        $users->increments('id')->unsigned();
        $users->string('first_name');
        $users->string('last_name');
        $users->string('username');
        $users->string('active_session', 128);
        $users->string('password', 128);
        $users->string('email', 200);
        $users->boolean('is_active');
        $users->timestamps()->nullable();
        $users->softDeletes()->nullable();
        $users->primary('id');
        $users->execute();

        $logs = new Schema('logs');
        $logs->increments('id')->unsigned();
        $logs->integer('userid')->unsigned()->nullable();
        $logs->integer('adminid')->unsigned()->nullable();
        $logs->string('log_for', 256);
        $logs->string('log_value', 512);
        $logs->string('log_state')->nullable();
        $logs->text('log_description')->nullable();
        $logs->ip('log_ip');
        $logs->timestamps()->nullable();
        $logs->softDeletes()->nullable();
        $logs->primary('id');
        $logs->foreign('userid')->references('id')->on('users');
        $logs->foreign('adminid')->references('id')->on('admins');
        $logs->execute();

        $sessions = new Schema('sessions');
        $sessions->string('id', 255);
        $sessions->int('timestamp',255);
        $sessions->text('data');
        $sessions->string('session_key', 255);
        $sessions->primary('id');
        $sessions->execute();

    }

    public function dropTables()
    {
        $tables_count = 0;
        foreach (self::$tables as $table) {
            if (Schema::dropTable($table)) {
                $tables_count++;
            }

        }
        if (count(self::$tables) == $tables_count) {
            return true;
        }

        return false;
    }
    public function rebuildTables()
    {
        $this->dropTables();
        $this->buildTables();

    }
    public static function register($requests)
    {
        $db      = new Model();
        $message = new FlashMessages();
        $gump    = new GUMP();
        $_POST   = $gump->sanitize($_POST);
        if (!isset($_POST['agreement'])) {
            $message->error('You must accept <a href="#" data-toggle="modal"
            data-target=".bs-example-modal-lg">agreement</a> to continue installation.');
            Redirect::back($_POST);
        }

        $is_valid = GUMP::is_valid($_POST, array(
            'firstname' => 'required|alpha_numeric|max_len,100|min_len,1',
            'lastname'  => 'required|alpha_numeric|max_len,100|min_len,1',
            'email'     => 'required|valid_email|max_len,200|min_len,4',
            'username'  => 'required|alpha_numeric|max_len,64|min_len,4',
            'mobile'    => 'required|alpha_numeric|max_len,11|min_len,11',
            'password'  => 'required|max_len,64|min_len,6'));

        $gump->filter_rules(array(
            'firstname' => 'trim|sanitize_string',
            'lastname'  => 'trim|sanitize_string',
            'password'  => 'trim',
            'email'     => 'trim|sanitize_email',
            'mobile'    => 'trim',
            'username'  => 'sanitize_string',
        ));
        $validated_data = $gump->run($_POST);
        if ($is_valid === true) {
            if ($validated_data === false) {
                $message->error($gump->getReadableErrors(true));
            } else {
                $time = new DateTime('now', TIMEZONE);
                $data = [
                    'first_name'    => $validated_data['firstname'],
                    'last_name'     => $validated_data['lastname'],
                    'email'         => $validated_data['email'],
                    'username'      => $validated_data['username'],
                    'mobile'        => $validated_data['mobile'],
                    'state'        => 1,
                    'password'      => password_hash($validated_data['password'], PASSWORD_BCRYPT),
                    'last_login_at' => $time->as_db,
                    'created_at'    => $time->as_db,
                    'last_login_ip' => IP_ADDRESS,
                    'access_level'  => 10,
                    'state'  => 0,];
                if ($db->insert('admins', $data)) {
                    $mail          = new Mail();
                    $mail->to      = $validated_data['email'];
                    $mail->subject = "Installation";
                    $mail->content = "Installation Successfull<br><br>Username:{$validated_data['username']}<br>Password:<i>hidden</i>";
                    $mail->sendMail();
                } else {

                    $message->error($db->getLastError()[2]);
                }
            }
            Redirect::back();
        } else {
            $message->error($is_valid[0]);
            Redirect::back($requests);
        }

    }

}
