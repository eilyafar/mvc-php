<?php
namespace Mini\Model;

use Mini\Core\DateTime;
use Mini\Core\FlashMessages;
use Mini\Core\GUMP;
use Mini\Core\Mail;
use Mini\Core\Model;
use Mini\Core\PDODb;
use Mini\Core\Redirect;
use Mini\Core\Securimage;
use Mini\Core\Session;
use Mini\Model\Admin;

class Login extends Model
{
    /**
     * @var object The database connection
     */
    private $db_connection = null;
    /**
     * @var array Collection of error messages
     */
    public $errors = array();
    /**
     * @var array Collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct()
    {

        if (isset($_GET["logout"])) {
            $this->doLogout();
        } elseif (isset($_POST["login"])) {
            $this->dologinWithPostData();
        }
    }
    public function validatePassword()
    {
        $admin   = new Admin();
        $ret     = ['return' => false, 'message' => 'Cannot proccess request.'];
        $db      = new PDODb();
        $gump    = new GUMP();
        $isValid = GUMP::is_valid($_POST, array(
            'password'       => 'required|max_len,62|min_len,6',
            'check_password' => 'required',
        ));
        if ($isValid === true) {
            $gump->filter_rules(array('password' => 'trim', 'check_password' => 'trim'));
            $validated_data = $gump->run($_POST);
            //$password = password_hash($validated_data['password'], PASSWORD_BCRYPT);
            $db->where('email', $admin->email());
            //$db->where('password', $password);
            if ($db_password = $db->getValue('admins', 'password')) {
                if (password_verify($validated_data['password'], $db_password)) {
                    $ret['return']  = true;
                    $ret['message'] = 'Successful, please wait...';
                } else {
                    $ret['message'] = 'Password incorrect.';
                }
            } else {
                $ret['message'] = 'User does not exists.';
            }
        } else {
            $ret['message'] = $isValid[0];
        }
        echo json_encode($ret);
    }
    public function recover($token = null, $email = null)
    {
        $db             = new PDODb();
        $gump           = new GUMP();
        $message        = new FlashMessages();
        $validated_data = null;
        if ($token == "store") {
            // if (!session_id()) {
            //     @session_start();
            // }
            $session = new Session();
            $db      = new PDODb();
            $message = new FlashMessages();
            $gump    = new GUMP();
            if (!GUMP::isValid_csrf()) {
                $_SESSION = array();
                $message->error("Invalid form security token. Did you submit the form twice by accident?");
            } else {

                $isValid = GUMP::is_valid($_POST, array(
                    'password'         => 'required|max_len,62|min_len,6',
                    'confirm_password' => 'required|max_len,62|min_len,6',
                    'recover_token'    => 'required|max_len,60|min_len,30',
                    'email'            => 'required|valid_email',
                ));
                if ($isValid === true) {
                    $gump->filter_rules(array(
                        'password'         => 'trim',
                        'confirm_password' => 'trim',
                        'recover_token'    => 'trim',
                        'email'            => 'trim|sanitize_email'));
                    $validated_data = $gump->run($_POST);
                    $recover_email  = $validated_data['email'];
                    $recover_token  = $validated_data['recover_token'];
                    if ($validated_data === false) {
                        $message->error($gump->get_readable_errors(true));
                        require APP . 'view/_templates/login_header.php';
                        require APP . 'view/dashboard/recover/newPassword.php';
                        require APP . 'view/_templates/login_footer.php';
                    } else {
                        $admin = new Admin($recover_email);
                        if ($admin->is_token_expired()) {
                            $message->error("Password reset token has been expired.");
                            Redirect::to(RECOVER);
                        } else {
                            $db->where('email', $recover_email);
                            $db->where('recover_token', $recover_token);
                            if ($db->has('admins')) {
                                if ($validated_data['password'] != $validated_data['confirm_password']) {
                                    $message->error("Password and confirm password are not same.");
                                    Redirect::back();
                                } else {
                                    $password = password_hash($validated_data['password'], PASSWORD_BCRYPT);
                                    $data     = ['recover_token' => 'not-set', 'password' => $password];
                                    $db->where('email', $recover_email);
                                    $db->where('recover_token', $recover_token);
                                    if ($db->update('admins', $data)) {
                                        $mail          = new Mail();
                                        $mail->to      = $email;
                                        $mail->to_name = $admin->fullName();
                                        $mail->subject = "Password Changed";
                                        $mail->content = "Your password has been changed successfully!";
                                        $mail->sendMail();
                                        $message->success("Your password has been changed successfully!");
                                        $_SESSION['password_change'] = true;
                                        Redirect::back();
                                    }
                                }
                            } else {
                                $message->error("Password reset token has been expired or account with that email exists.");
                            }
                        }
                    }
                } else {
                    $message->error($isValid[0]);
                    Redirect::back();
                }
            }
        } elseif ($token == null) {
            $title = "Recover Password";
            require APP . 'view/_templates/login_header.php';
            require APP . 'view/dashboard/recover/index.php';
            require APP . 'view/_templates/login_footer.php';
        } elseif (strlen($token) > 29 and $email != null) {
            $get     = ['token' => $token, 'email' => $email];
            $isValid = GUMP::is_valid($get, array(
                'token' => 'required|max_len,60|min_len,30',
                'email' => 'required|valid_email',
            ));
            if ($isValid === true) {
                $gump->filter_rules(array(
                    'token' => 'trim',
                    'email' => 'trim|sanitize_email'));
                $validated_data = $gump->run($get);
                $recover_email  = $validated_data['email'];
                $recover_token  = $validated_data['token'];
                if ($validated_data === false) {
                    $message->error($gump->get_readable_errors(true));
                    Redirect::to(RECOVER);
                }
                $admin = new Admin($recover_email);
                $db->where('email', $recover_email);
                $db->where('recover_token', $recover_token);
                if ($db->has('admins') and !$admin->is_token_expired()) {
                    $title         = "New Password";
                    $email_address = $recover_email;
                    require APP . 'view/_templates/login_header.php';
                    require APP . 'view/dashboard/recover/newPassword.php';
                    require APP . 'view/_templates/login_footer.php';
                } else {
                    $message->error('User does not exists.');
                    Redirect::to(RECOVER);
                }
            } else {
                require APP . 'view/_templates/login_header.php';
                require APP . 'view/dashboard/recover/index.php';
                require APP . 'view/_templates/login_footer.php';
            }
        } else {
            $message->error('Error da');
            Redirect::to(RECOVER);
        }
    }

    /**
     * log in with post data
     */
    private function dologinWithPostData()
    {
        $session = new Session();
        if (empty($_POST['username'])) {
            $this->errors[] = "Username field was empty.";
        } elseif (empty($_POST['password'])) {
            $this->errors[] = "Password field was empty.";
        } elseif (!empty($_POST['username']) && !empty($_POST['password'])) {
            $username = $this->cleanInput($_POST['username']);
            $this->where('username', $username);
            $this->where("deleted_at IS NULL");
            $user = $this->getOne('admins');
            if ($user) {

                if (password_verify($_POST['password'], $user['password'])) {
                    if (isset($_POST['rememberme'])) {
                        setcookie('rememberme', $this->generateRememberString($user['password']), REMEMBERME_EXPIRY_TIME);
                        setcookie('username', $user['username'], REMEMBERME_EXPIRY_TIME);
                        setcookie('user_login_status', 'logged', REMEMBERME_EXPIRY_TIME);
                    } elseif (!isset($_POST['rememberme'])) {
                        if (isset($_COOKIE['rememberme'])) {
                            setcookie('rememberme', 'gone', PAST_TIME);
                            setcookie('username', 'gone', PAST_TIME);
                            setcookie('user_login_status', 'notlogged', PAST_TIME);
                        }
                    }
                    $time                          = new DateTime('now', TIMEZONE);
                    $admin                         = new Admin();
                    $_SESSION['last_activity']     = time();
                    $_SESSION['username']          = $user['username'];
                    $_SESSION['email']             = $user['email'];
                    $_SESSION['user_login_status'] = 1;
                    $_SESSION['user_login_string'] = $this->generateLoginString($user['password']);
                    $admin->updateLastLogin($user['id']);
                    $data = [
                        'active_session' => $this->generateLoginString($user['password']),
                        'last_login_at'  => $time->as_db,
                        'last_login_ip'  => IP_ADDRESS];

                    $this->where('username', $username);
                    $this->update('admins', $data);
                    $admin->addAttempt('login', 'success');

                } else {
                    //$this->addAttempt($user['id'],"login","failed","wrong password");
                    $this->errors[] = "The username or password you entered is incorrect.";
                }
            } else {
                $this->errors[] = "The username or password you entered is incorrect.";
            }
        }
    }
    public function generateLoginString($password_hash)
    {
        return hash('sha512', $password_hash . IP_ADDRESS . BROWSER . BROWSER_VERSION . SECURITY_KEY);
    }
    public function generateRememberString($password_hash)
    {
        return hash('sha512', $password_hash . SECURITY_KEY);
    }
    public function checkEmail($post)
    {
        // if (!session_id()) {
        //     @session_start();
        // }
        $session = new Session();
        $message = new FlashMessages();
        $gump    = new GUMP();

        if (!GUMP::isValid_csrf()) {
            $_SESSION = array();
            $message  = new FlashMessages();
            $message->error("Invalid form security token. Did you submit the form twice by accident?");
        } else {
            $isValid = GUMP::is_valid($post, array(
                'email' => 'required|valid_email',
            ));
            if ($isValid === true) {
                $gump->filter_rules(array('email' => 'trim|sanitize_email'));
                $validated_data = $gump->run($post);
                if ($validated_data === false) {
                    $message->error($gump->get_readable_errors(true));
                    Redirect::back();
                } else {
                    $email = $validated_data['email'];
                    $admin = new Admin($email);
                    $this->where("deleted_at IS NULL");
                    $this->where('email', $email);
                    if ($this->has('admins')) {
                        $recover_token = RECOVER_TOKEN;
                        $time          = new DateTime('now', 'Asia/Tehran');
                        $data          = ['recover_token' => $recover_token, 'recover_token_at' => $time->as_db];
                        $this->where('email', $email);
                        if ($this->update('admins', $data)) {
                            $mail          = new Mail();
                            $mail->to      = $email;
                            $mail->to_name = $admin->fullName();
                            $mail->subject = "Recover Password";
                            $mail->content = "To recover password click on reset link.<br><a href='" . RECOVER .
                                "/$recover_token/{$email}'>" . RECOVER . "/$recover_token/{$email}</a>";
                            $mail->sendMail();
                            $message->success("Check your email inbox and click the reset link in that email.<a onClick='resend()'' href='#'>
                                <strong>Not send? click to resend</strong> </a>");
                        }
                    } else {
                        $message->error("No account with that email exists.");
                    }
                }
            }
        }
        Redirect::back($post);
    }
    public function checkLogin($post)
    {
        $session    = new Session();
        $message    = new FlashMessages();
        $securimage = new Securimage();
        if (!GUMP::isValidCsrf()) {
            $_SESSION = array();
            $message  = new FlashMessages();
            $message->error("Invalid form security token. Did you submit the form twice by accident?");
            Redirect::back();
        } else {

            if (CAPTCHA and $securimage->check($post['captcha_code']) == false) {
                $message->error("The security code entered was incorrect.");
                Redirect::back();
            } else {
                $isValid = GUMP::is_valid($post, array(
                    'username' => 'required|alpha_numeric|max_len,100|min_len,4',
                    'password' => 'required|max_len,100|min_len,6',
                ));
                if ($isValid === true) {
                    $login = new Login();
                    if ($login->isUserLoggedIn() == true) {
                        if (isset($_SESSION['redirectto'])) {
                            $path = $_SESSION['redirectto'];
                            unset($_SESSION['redirectto']);
                            Redirect::to($path);
                        }
                        Redirect::to(DASHBOARD);
                    } else {
                        $message->error($login->errors[0]);
                        Redirect::back();
                    }
                } else {
                    $message->error($isValid[0]);
                    Redirect::back();
                }
            }
        }
    }
    public function addAttempt($userid, $log_for, $log_value, $description = null, $log_state = "active")
    {
        $data = ["log_for" => $log_for,
            "log_value"        => $log_value,
            "log_state"        => $log_state,
            "log_description"  => $description,
            "log_ip"           => IP_ADDRESS,
            "created_at"       => date("Y-m-d H:i:s"),
            "userid"           => $userid];
        $this->where('log_for', 'login');
        $this->where('log_value', 'success');
        $this->where('log_state', 'active');
        $this->where('log_ip', IP_ADDRESS);
        $log = $this->getOne('logs');
        if ((time() - strtotime($log['created_at'])) < 10) {
            return false;
        }

        return $this->insert('logs', $data);
    }
    /**
     * perform the logout
     */
    public function doLogout()
    {
        $session  = new Session();
        $_SESSION = array();
        session_destroy();
        $this->messages[] = "You have been logged out.";
        if (isset($_COOKIE['user_login_status'])) {
            setcookie('user_login_status', 'notlogged', PAST_TIME);
        }
        Redirect::to(DASHBOARD);
    }

    /**
     * simply return the current state of the user's login
     * @return boolean user's login status
     */
    public function isUserLoggedIn()
    {

        $session = new Session();
        if (isset($_COOKIE['rememberme'], $_COOKIE['username'], $_COOKIE['user_login_status'])
            and $_COOKIE['user_login_status'] == 'logged') {
            $username = $_COOKIE['username'];
            $this->where("deleted_at IS NULL");
            $this->where('username', $username);
            $user = $this->getOne('admins');
            if ($user) {
                if ($this->generateRememberString($user['password']) == $_COOKIE['rememberme']) {
                    return true;
                }
            }
        } else {
            if (isset($_SESSION['user_login_status'], $_SESSION['username'], $_SESSION['user_login_string'])
                and $_SESSION['user_login_status'] == 1) {
                if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] < LOGIN_EXPIRY_TIME)) {
                    $username = $_SESSION['username'];
                    $this->where("deleted_at IS NULL");
                    $this->where('username', $username);
                    $user = $this->getOne('admins');
                    if ($user) {
                        $user_login_string = LOGIN_MULTIPLE_ADMINS ? $_SESSION['user_login_string'] : $user['active_session'];
                        if ($this->generateLoginString($user['password']) == $user_login_string) {
                            $_SESSION['last_activity'] = time();
                            if ($this->generateLoginString($user['password']) == $user['active_session']) {

                                $_SESSION['multi_login'] = false;
                            } else {
                                $data                    = ["ip" => IP_ADDRESS, "browser" => BROWSER];
                                $_SESSION['multi_login'] = json_encode($data);
                            }
                            return true;
                        }
                    }
                } else {
                    $message = new FlashMessages();
                    $message->setCloseBtn();
                    $message->error("The login session has been expired.");
                }
            }
        }
        return false;
    }
}
