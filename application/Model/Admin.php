<?php
namespace Mini\Model;

use Mini\Core\DateTime;
use Mini\Core\Encryption;
use Mini\Core\FlashMessages;
use Mini\Core\GUMP;
use Mini\Core\Mail;
use Mini\Core\Model;
use Mini\Core\Session;

class Admin extends Model
{
    private $username = null;
    private $id       = 0;
    public $user      = null;
    public $ignore    = ['id', 'password', 'active_session', 'recover_token_at', 'recover_token'];

    public function __construct($username_or_email = null)
    {
        $session  = new Session();
        $username = null;
        if ($username_or_email == null and isset($_SESSION['username'])) {
            $username = $_SESSION['username'];
        } elseif ($username_or_email != null) {
            $username = $username_or_email;
        }
        $this->where('username', $username);
        $this->orWhere('email', $username);
        $user = $this->getOne('admins');
        if ($user) {
            $this->username = $user['username'];
            $this->id       = $user['id'];
            $this->user     = $user;
        }
    }
    public function getAllAdmins()
    {
        $this->where("deleted_at IS NULL");
        return $this->get('admins');
    }
    public function deleteAdmin()
    {
        $time       = new DateTime('now', TIMEZONE);
        $ret        = ["return" => false,"login"=>true, "message" => "Cannot process this request."];
        $numeric_id = $_POST['id'];
        if (!is_numeric($numeric_id)) {
            $numeric_id = Encryption::decrypt($numeric_id, $_POST['key']);
        }
        if($this->countAdmins()>1)
        {
            $this->where('id', $numeric_id);
            $data = ["deleted_at" => $time->as_db];
            if ($this->update('admins', $data)) {
                $ret['return']  = true;
                $ret['message'] = "Delete successfull.";
            }
        }
        else
        {
            $ret["message"]="admins count <= 1";
        }
        echo json_encode($ret);
    }
    protected function hasAdmin($id)
    {
        if ($this->find($id) == false) {
            return false;
        }
        return true;
    }
    public function countAdmins()
    {
        $this->where("deleted_at IS NULL");
        return $this->getValue("admins", "count(*)");
    }
    public function find($id)
    {
        $session = new Session();
        $date    = new DateTime();
        $this->where('id', $id);
        if ($admin = $this->getOne('admins')) {
            if ($admin['updated_at'] === null) {
                $admin['modal_title'] = $admin['first_name'] . " " . $admin["last_name"] . " <small>Created " . $date->formatTime($admin['created_at']) . "</small>";
            } else {
                $admin['modal_title'] = $admin['first_name'] . " " . $admin["last_name"] . "  <small>Updated " . $date->formatTime($admin['updated_at']) . "</small>";
            }
            $key                    = RANDOM_STRING;
            $admin['encrypted_id']  = Encryption::encrypt($admin['id'], $key);
            $admin['encrypted_key'] = $key;
            $admin['fullname']      = $admin['first_name'] . " " . $admin['last_name'];
            $_SESSION['csrf_token'] = CSRF_TOKEN;
            $admin['csrf_token']    = $_SESSION['csrf_token'];
            $random_id              = RANDOM_STRING;
            $actions                = '<div style="text-align: right;" id="td-' . $random_id . '" class="btn-group" ><button type="button" class="btn btn-info"' .
                'data-toggle="modal" onClick="fetch_admin_info(\'' . $admin['encrypted_id'] . '\',\'' . $key .
                '\',\'' . AJAX . '/fetch_admin\')" ' .
                'data-target="#update-admin-modal">Edit</button>' .
                '<button onClick="delete_admin(\'' . $admin['encrypted_id'] . '\',\'' . $key .
                '\',\'' . AJAX . '/delete_admin\',\'' . $random_id . '\')" type="button" role="button"  class="btn btn-danger">Trah</button>';

            $admin["actions"] = $actions;
            foreach ($this->ignore as $field) {
                unset($admin[$field]);
            }
            return $admin;
        }
        return false;
    }
    public function firstName()
    {
        return $this->user['first_name'];
    }
    public function username()
    {
        return $this->user['username'];
    }
    public function mobile()
    {
        return $this->user['mobile'];
    }
    public function email()
    {
        return $this->user['email'];
    }
    public function lastName()
    {
        return $this->user['last_name'];
    }
    public function isTokenExpired()
    {
        $time     = new DateTime('now', TIMEZONE);
        $token_at = strtotime($this->user['recover_token_at']);
        $now      = strtotime($time->as_db);
        if ((RECOVER_EXPIRATION - ($now - $token_at)) > 0) {
            return false;
        }
        return true;
    }
    public function updateLastLogin($id)
    {
        $this->id = $id;
        $time     = new DateTime('now', TIMEZONE);
        $this->where('log_for', 'last_login_at');

        if ($id = $this->getOne('logs')) {
            $this->updateAttempt($id['id'], 'last_login_at', $this->user['last_login_at']);
        } else {
            $this->addAttempt('last_login_at', $time->as_db);
        }
        $this->where('log_for', 'last_login_ip');
        if ($id = $this->getOne('logs')) {
            $this->updateAttempt($id['id'], 'last_login_ip', $this->user['last_login_ip']);
        } else {
            $this->addAttempt('last_login_ip', $this->user['last_login_ip']);
        }
    }
    public function addAttempt($log_for, $log_value, $description = null, $log_state = "active")
    {
        $time = new DateTime('now', TIMEZONE);
        $data = [
            "log_for"         => $log_for,
            "log_value"       => $log_value,
            "log_state"       => $log_state,
            "log_description" => $description,
            "log_ip"          => IP_ADDRESS,
            "created_at"      => $time->as_db,
            "updated_at"      => $time->as_db,
            "adminid"         => $this->id];
        $this->insert('logs', $data);
    }
    public function updateAttempt($id, $log_for, $log_value, $description = null, $log_state = "active")
    {
        $time = new DateTime('now', TIMEZONE);
        $data = [
            "log_for"         => $log_for,
            "log_value"       => $log_value,
            "log_state"       => $log_state,
            "log_description" => $description,
            "log_ip"          => IP_ADDRESS,
            "updated_at"      => $time->as_db,
            "adminid"         => $this->id];
        $this->where('id', $id);
        return $this->update('logs', $data);
    }
    public function getLastLogin()
    {
        $time = new DateTime('now', TIMEZONE);
        $ret  = ['ip' => null, 'at' => null];
        $this->where('adminid', $this->id);
        $this->where('log_for', 'last_login_at');
        if ($log = $this->getOne('logs')) {
            $ret['at'] = $log['log_value'];
        } else {
            $ret['at'] = $time->as_db;
        }
        $this->where('adminid', $this->id);
        $this->where('log_for', 'last_login_ip');
        if ($log = $this->getOne('logs')) {
            $ret['ip'] = $log['log_value'];
        } else {
            $ret['ip'] = IP_ADDRESS;
        }
        return $ret;
    }
    public function fullName()
    {
        return $this->user['first_name'] . " " . $this->user['last_name'];
    }
    public function shortName()
    {
        return strtoupper(substr($this->user['first_name'], 0, 1) . substr($this->user['last_name'], 0, 1));
    }
    public function updateAdmin($id = false)
    {
        $ret     = ["return" => false,"login"=>true, "message" => "Cannot process this request."];
        $gump    = new GUMP();
        $db      = new Model();
        $message = new FlashMessages();
        if ($id or GUMP::isValidPassword($_POST['username'], $_POST['password'])) {
            $id = Encryption::decrypt($_POST['id'], $_POST['key']);

            $_POST    = $gump->sanitize($_POST);
            $is_valid = GUMP::is_valid($_POST, array(
                'csrf'      => 'required|alpha_numeric',
                'firstname' => 'required|alpha_numeric|max_len,100|min_len,1',
                'lastname'  => 'required|alpha_numeric|max_len,100|min_len,1',
                'email'     => 'required|valid_email|max_len,200|min_len,4',
                'username'  => 'required|alpha_numeric|max_len,64|min_len,4',
                'mobile'    => 'required|alpha_numeric|max_len,11|min_len,11',
            ));
            $gump->filter_rules(array(
                'csrf'      => 'trim|sanitize_string',
                'firstname' => 'trim|sanitize_string',
                'lastname'  => 'trim|sanitize_string',
                'email'     => 'trim|sanitize_email',
                'mobile'    => 'trim',
                'username'  => 'sanitize_string',
            ));
            $validated_data = $gump->run($_POST);
            if (GUMP::isValidCsrf()) {
                if ($is_valid === true) {
                    if ($validated_data === false) {
                        $ret["return"]  = false;
                        $ret["message"] = $gump->getReadableErrors(true);
                        //$message->error($gump->getReadableErrors(true));
                    } else {
                        $time = new DateTime('now', TIMEZONE);
                        $data = [
                            'first_name' => $validated_data['firstname'],
                            'last_name'  => $validated_data['lastname'],
                            'email'      => $validated_data['email'],
                            'username'   => $validated_data['username'],
                            'mobile'     => $validated_data['mobile'],
                            'updated_at' => $time->as_db];
                        $db->where('id', $id);

                        if ($db->update('admins', $data)) {
                            $mail          = new Mail();
                            $mail->to      = $validated_data['email'];
                            $mail->to_name = $this->fullName();
                            $mail->subject = "Updated";
                            $mail->content = "Profile information was update successfull<br><br>Username:{$validated_data['username']}<br>
                    Email:{$validated_data['email']}";
                            //$mail->sendMail();
                            //$message->success('Profail information was update successfull.');
                            $ret["return"]  = true;
                            $ret["message"] = "Profile information was update successfull";
                        } else {
                            $ret["return"]  = false;
                            $ret["message"] = "Problem in update admin, try again.";
                            //$message->error('Problem in update admin, try again.');
                        }
                    }
                    //Redirect::back();
                } else {
                    //$message->error($is_valid[0]);
                    //Redirect::back();
                    $ret["return"]  = false;
                    $ret["message"] = $is_valid[0];
                }
            } else {
                $ret["return"]  = false;
                $ret["message"] = "csrf token is invalid.";
            }
        } else {
            // $message->error('Password incorrect.');
            // Redirect::back();
            $ret["return"]  = false;
            $ret["message"] = "Password incorrect.";
        }

        echo json_encode($ret);
    }

    public function addAdmin()
    {
        $ret     = ["return" => false, "login" => true, "message" => "Cannot process this request.", "logged" => true, "admin" => ""];
        $gump    = new GUMP();
        $db      = new Model();
        $message = new FlashMessages();

        $_POST    = $gump->sanitize($_POST);
        $is_valid = GUMP::is_valid($_POST, array(
            'csrf_token'    => 'required|alpha_numeric',
            'new_firstname' => 'required|alpha_numeric|max_len,100|min_len,1',
            'new_password'  => 'required|max_len,100|min_len,6',
            'new_lastname'  => 'required|alpha_numeric|max_len,100|min_len,1',
            'new_email'     => 'required|valid_email|max_len,200|min_len,4',
            'new_username'  => 'required|alpha_numeric|max_len,64|min_len,4',
            'new_mobile'    => 'required|alpha_numeric|max_len,11|min_len,11',
        ));
        $gump->filter_rules(array(
            'csrf_token'    => 'trim|sanitize_string',
            'new_firstname' => 'trim|sanitize_string',
            'new_lastname'  => 'trim|sanitize_string',
            'new_email'     => 'trim|sanitize_email',
            'new_mobile'    => 'trim',
            'new_password'  => 'trim',
            'new_username'  => 'sanitize_string',
        ));
        $validated_data = $gump->run($_POST);
        if (GUMP::isValidCsrf()) {
            if ($is_valid === true) {
                if ($validated_data === false) {
                    $ret["return"]  = false;
                    $ret["message"] = $gump->getReadableErrors(true);
                    //$message->error($gump->getReadableErrors(true));
                } else {
                    $time = new DateTime('now', TIMEZONE);
                    $data = [
                        'first_name'   => $validated_data['new_firstname'],
                        'last_name'    => $validated_data['new_lastname'],
                        'email'        => $validated_data['new_email'],
                        'access_level' => 10,
                        'username'     => $validated_data['new_username'],
                        'password'     => password_hash($validated_data['new_password'], PASSWORD_BCRYPT),
                        'mobile'       => $validated_data['new_mobile'],
                        'state'       => 0,
                        'created_at'   => $time->as_db];

                    if ($inserted = $db->insert('admins', $data)) {
                        $ret["admin"]  = $this->find($inserted);
                        $mail          = new Mail();
                        $mail->to      = $validated_data['new_email'];
                        $mail->to_name = $this->fullName();
                        $mail->subject = "Updated";
                        $mail->content = "Profile was created successfull<br><br>Username:{$validated_data['new_username']}<br>
                    Email:{$validated_data['new_email']}";
                        //$mail->sendMail();
                        //$message->success('Profail information was update successfull.');
                        $ret["return"]  = true;
                        $ret["message"] = "Profile information was add successfull";
                    } else {
                        $ret["return"]  = false;
                        $ret["message"] = $db->getLastError();
                        //$message->error('Problem in update admin, try again.');
                    }
                }
            } else {
                $ret["return"]  = false;
                $ret["message"] = $is_valid[0];
            }
        } else {
            $ret["return"]  = false;
            $ret["message"] = "csrf token is invalid.";
        }

        echo json_encode($ret);
    }

    public function changePassword()
    {
        $ret      = ['return' => false, 'message' => 'Cannot proccess request.'];
        $gump     = new GUMP();
        $db       = new Model();
        $message  = new FlashMessages();
        $_POST    = $gump->sanitize($_POST);
        $is_valid = GUMP::is_valid($_POST, array(
            'old_password'     => 'required|max_len,100|min_len,6',
            'new_password'     => 'required|max_len,100|min_len,6',
            'confirm_password' => 'required|max_len,100|min_len,6'));
        $gump->filter_rules(array(
            'old_password'     => 'trim',
            'new_password'     => 'trim',
            'confirm_password' => 'trim'));
        $validated_data = $gump->run($_POST);
        if ($validated_data === false) {
            $ret['message'] = $gump->getReadableErrors(true);
        } else {
            if ($is_valid === true) {

                if (GUMP::isValidPassword($this->username, $_POST['old_password'])) {
                    $time = new DateTime('now', TIMEZONE);
                    $data = [
                        'password'   => password_hash($validated_data['new_password'], PASSWORD_BCRYPT),
                        'updated_at' => $time->as_db];
                    $db->where('id', $this->id);
                    if ($db->update('admins', $data)) {
                        $mail          = new Mail();
                        $mail->to      = $this->user['email'];
                        $mail->to_name = $this->fullName();
                        $mail->subject = "Password Change";
                        $mail->content = "Passwrord change was successfull<br><br>Username:{$this->user['username']}";
                        $mail->sendMail();
                        $message->success('Password change was successfull.');
                        $ret['return']  = true;
                        $ret['message'] = 'Password change was successfull.';
                    } else {
                        $ret['message'] = 'Problem in change password, try again.';
                    }
                } else {
                    $ret['message'] = 'Current password incorrect.';
                }
            } else {
                $ret['message'] = $gump->getReadableErrors(true);
            }
        }
        echo json_encode($ret);
    }

    public function fetchAdmin()
    {
        $ret     = ['return' => false, 'message' => 'Cannot proccess request.', 'admin' => ''];
        $gump    = new GUMP();
        $db      = new Model();
        $message = new FlashMessages();
        $id      = Encryption::decrypt($_POST['id'], $_POST['key']);
        if ($this->find($id) == false) {
            $ret['return']  = false;
            $ret['message'] = "User does not exists.";
        } else {
            $ret['return']  = true;
            $ret['message'] = "Data load successfull";
            $ret['admin']   = $this->find($id);
        }
        echo json_encode($ret);
    }
}
